package com.example.dima.catalogapplication.mortar;

import android.support.annotation.Nullable;
import android.util.Log;

import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.flow.AbstractScreen;

import java.lang.reflect.ParameterizedType;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import mortar.MortarScope;

/**
 * @user dima
 * @date 29.11.2016.
 * Класс, возвращающий конкретный MortarScope для конкретного экрана.
 * Сюда передается экран, а в ответ - область видимости
 */

public class ScreenScoper {
    private static final String TAG = ScreenScoper.class.getName();
    private static Map<String, MortarScope> sScopeMap = new HashMap<>();

    public static MortarScope getScreenScope(AbstractScreen screen) {
        if (!sScopeMap.containsKey(screen.getScopeName())) {
            Log.d(TAG, "getScreenScope: create new scope");
            return createScreenScope(screen);
        } else {
            Log.d(TAG, "getScreenScope: return exist scope");
            return sScopeMap.get(screen.getScopeName());
        }
    }

    public static void registerScope(MortarScope mortarScope) {
        sScopeMap.put(mortarScope.getName(), mortarScope);
    }

    private static void cleanScopeMap() {
        Iterator<Map.Entry<String, MortarScope>> iterator = sScopeMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, MortarScope> entry = iterator.next();
            if (entry.getValue().isDestroyed()) {
                iterator.remove();
            }
        }
    }

    public static void destroyScreenScope(String scopeName) {
        MortarScope mortarScope = sScopeMap.get(scopeName);
        mortarScope.destroy();
        cleanScopeMap();
    }

    @Nullable
    private static String getParentScopeName(AbstractScreen screen) {
        try {
            String genericName = ((Class) ((ParameterizedType) screen.getClass().getGenericSuperclass())
                    .getActualTypeArguments()[0]).getName();
            String parentScopeName = genericName;

            if (parentScopeName.contains("$")) {
                parentScopeName = parentScopeName.substring(0, genericName.indexOf("$"));
            }
            return parentScopeName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Метод создает область видимости для нашего экрана
     * @param screen экран, KeyObject, ValueObject, для которого необходимо создать область видимости
     * @return
     */
    private static MortarScope createScreenScope(AbstractScreen screen) {
        Log.d(TAG, "createScreenScope: with name " + screen.getScopeName());
        MortarScope parentScope = sScopeMap.get(getParentScopeName(screen));
        Object screenComponent = screen.createScreenComponent(parentScope.getService(DaggerService.SERVICE_NAME));
        MortarScope newScope = parentScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, screenComponent)
                .build(screen.getScopeName());
        registerScope(newScope);
        return newScope;
    }
}
