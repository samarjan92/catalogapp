package com.example.dima.catalogapplication.feature.catalog;

import com.example.dima.catalogapplication.base.IBaseView;
import com.example.dima.catalogapplication.data.storage.dto.ProductDto;

import java.util.List;

/**
 * @user dima
 * @date 30.10.2016.
 */

public class CatalogContract {

    public interface IView extends IBaseView {
        void showCatalogView(List<ProductDto> productList);
        void updateProductCounter();
    }

    public interface IPresenter {
        void clickOnBuyButton(int position);
        boolean checkUserAuth();

    }
}
