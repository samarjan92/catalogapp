package com.example.dima.catalogapplication.feature.authentication;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.CheckResult;
import android.support.annotation.IdRes;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.dima.catalogapplication.R;

/**
 * Created by dima on 23.10.2016.
 *
 */

public class AuthPanel extends LinearLayout {
    private static final String TAG = AuthPanel.class.getName();

    private static final String EMPTY_STRING = "";

    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;

    private int mCustomState = IDLE_STATE;
    private boolean isLoading = false;
    private final boolean useNewInterpolators = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;

    private CardView mAuthCard;
    private Button mLoginBtn;
    private Button mShowCatalogBtn;
    private EditText mLoginEmailEt;
    private EditText mLoginPasswordEt;
    private TextInputLayout mLoginEmailEtWrapper;
    private TextInputLayout mLoginPasswordEtWrapper;

    public AuthPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mLoginBtn = findById(R.id.enter_btn);
        mShowCatalogBtn = findById(R.id.show_catalog_btn);
        mLoginEmailEt = findById(R.id.login_email_et);
        mLoginPasswordEt = findById(R.id.login_password_et);
        mLoginEmailEtWrapper = findById(R.id.login_email_et_wrap);
        mLoginPasswordEtWrapper = findById(R.id.login_password_et_wrap);
        mAuthCard = findById(R.id.auth_card);
        showViewFromState();
    }

    public void setCustomState(int state) {
        mCustomState = state;
        showViewFromState();
    }

    private void showLoginState() {
       animateHideAuthCard();
        mShowCatalogBtn.setVisibility(GONE);
    }

    private void showIdleState() {
        animateShowAuthCard();
        mShowCatalogBtn.setVisibility(VISIBLE);
    }

    private void animateHideAuthCard() {
        mAuthCard.setScaleX(0f);
        mAuthCard.setScaleY(0f);
        mAuthCard.setVisibility(VISIBLE);
        Interpolator interpolator = AnimationUtils.loadInterpolator(this.getContext(),
                useNewInterpolators ?
                        android.R.interpolator.fast_out_slow_in : android.R.interpolator.accelerate_decelerate);
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(mAuthCard, View.SCALE_X, (1f));
        scaleX.setInterpolator(interpolator);
        scaleX.setDuration(200L);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(mAuthCard, View.SCALE_Y, (1f));
        scaleY.setInterpolator(interpolator);
        scaleY.setDuration(600L);
        scaleX.start();
        scaleY.start();
    }

    private void animateShowAuthCard() {
        Interpolator interpolator = AnimationUtils.loadInterpolator(this.getContext(),
                useNewInterpolators ?
                        android.R.interpolator.fast_out_slow_in : android.R.interpolator.accelerate_decelerate);
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(mAuthCard, View.SCALE_X, (0f));
        scaleX.setInterpolator(interpolator);
        scaleX.setDuration(200L);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(mAuthCard, View.SCALE_Y, (0f));
        scaleY.setInterpolator(interpolator);
        scaleY.setDuration(600L);
        scaleX.start();
        scaleY.start();
        //        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mAuthCard.setVisibility(GONE);
//            }
//        }, 600);
    }

    private void showViewFromState() {
        if (mCustomState == LOGIN_STATE) {
            showLoginState();
        } else {
            showIdleState();
        }
    }

    private void showLoadingState() {
        if (isLoading()) {
            disableButtons();
        } else {
            enableButtons();
        }
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
        showLoadingState();
    }

    private void disableButtons() {
        mLoginBtn.setEnabled(false);
        mShowCatalogBtn.setEnabled(false);
    }

    private void enableButtons() {
        mLoginBtn.setEnabled(true);
        mShowCatalogBtn.setEnabled(true);
    }

    public String getUserEmail() {
        return String.valueOf(mLoginEmailEt.getText());
    }

    public String getUserPassword() {
        return String.valueOf(mLoginPasswordEt.getText());
    }

    public boolean isIdle() {
        return mCustomState == IDLE_STATE;
    }

    public void showEmailError(String errorMessage) {
        mLoginEmailEtWrapper.setErrorEnabled(true);
        mLoginEmailEtWrapper.setError(errorMessage);
    }

    public void hideEmailError() {
        mLoginEmailEtWrapper.setError(EMPTY_STRING);
        mLoginEmailEtWrapper.setErrorEnabled(false);
    }

    public void showPasswordError(String errorMessage) {
        mLoginPasswordEtWrapper.setErrorEnabled(true);
        mLoginPasswordEtWrapper.setError(errorMessage);
    }

    public void hidePasswordError() {
        mLoginPasswordEtWrapper.setError(EMPTY_STRING);
        mLoginPasswordEtWrapper.setErrorEnabled(false);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.state = mCustomState;
        savedState.loading = isLoading;
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCustomState(savedState.state);
        setLoading(savedState.loading);
    }

    //Суперское решение, но символ $ напоминает мне jQuery
    @SuppressWarnings({ "unchecked", "UnusedDeclaration" }) // Checked by runtime cast. Public API.
    @CheckResult
    protected <T extends View> T findById(@IdRes int id) {
        return (T) findViewById(id);
    }

    static class SavedState extends BaseSavedState {
        private int state;
        private boolean loading;

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

            @Override
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        SavedState(Parcelable superState) {
            super(superState);
        }

        SavedState(Parcel in) {
            super(in);
            state = in.readInt();
            loading = in.readByte() != 0;
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(state);
            out.writeByte((byte) (loading ? 1 : 0));
        }
    }
}
