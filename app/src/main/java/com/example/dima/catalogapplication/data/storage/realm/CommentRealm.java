package com.example.dima.catalogapplication.data.storage.realm;

import com.example.dima.catalogapplication.data.network.response.CommentResponse;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @user dima
 * @date 07.01.2017.
 */

public class CommentRealm extends RealmObject {
    @PrimaryKey
    private String id;
    private String userName;
    private String avatar;
    private float rating;
    private Date commentDate;
    private String comment;

    //default realm constructor is required
    public CommentRealm() {
    }

    public CommentRealm(CommentResponse commentResponse) {
        this.id = commentResponse.id;
        this.userName = commentResponse.userName;
        this.avatar = commentResponse.avatar;
        this.rating = commentResponse.raiting;
        this.commentDate = new Date(commentResponse.commentDate);
        this.comment = commentResponse.comment;
    }

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public float getRating() {
        return rating;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public String getComment() {
        return comment;
    }
}
