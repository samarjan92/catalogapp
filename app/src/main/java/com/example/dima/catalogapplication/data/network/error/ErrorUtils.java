package com.example.dima.catalogapplication.data.network.error;

import com.example.dima.catalogapplication.data.DataManager;

import java.io.IOException;

import retrofit2.Response;

/**
 * @user dima
 * @date 20.12.2016.
 */

public class ErrorUtils {

    public static ApiError parseError(Response<?> errorResponse) {
        ApiError error;
        try {
            error = (ApiError) DataManager.getInstance().getRetrofit()
                    .responseBodyConverter(ApiError.class, ApiError.class.getAnnotations())
                    .convert(errorResponse.errorBody());
        } catch (IOException e) {
            return new ApiError();
        }
        return error;
    }
}
