package com.example.dima.catalogapplication.ui.screens.account;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.CoordinatorLayout;
import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.data.storage.dto.UserDto;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.mvp.view.IAccountView;
import com.squareup.picasso.Picasso;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import flow.Flow;

/**
 * @user dima
 * @date 06.12.2016.
 */

public class AccountView extends CoordinatorLayout implements IAccountView {
    static final int PREVIEW_STATE = 1;
    static final int EDIT_STATE = 0;

    @Inject
    AccountScreen.AccountPresenter mPresenter;
    @Inject
    Picasso mPicasso;

    @BindView(R.id.user_avatar)
    CircleImageView mUserAvatar;
    @BindView(R.id.take_photo_button)
    ImageView mTakePhotoButton;
    @BindView(R.id.add_address_button)
    Button mAddAddressButton;
    @BindView(R.id.order_notification_switch)
    SwitchCompat mOrderNotificationSwitch;
    @BindView(R.id.action_notification_switch)
    SwitchCompat mActionNotificationSwitch;
    @BindView(R.id.address_list)
    RecyclerView mAddressList;
    @BindView(R.id.profile_name)
    TextView mProfileName;
    @BindView(R.id.profile_phone)
    TextView mProfilePhone;
    @BindView(R.id.fab)
    FloatingActionButton mFab;

    private AccountScreen mScreen;
    private UserDto mUserDto;

    public AccountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
            DaggerService.<AccountScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region ==================== LifeCycle ====================

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    private void showViewFromState() {
        if (mScreen.getCustomState() == EDIT_STATE) {
            showEditState();
            if (mScreen.isDialogShowed()) {
                showPhotoSourceDialog();
            }
        } else  {
            showPreviewState();
        }
    }

    public void initView(UserDto userDto) {
        mUserDto = userDto;
        initProfileInfo();
        initList();
        initSettings();
        showViewFromState();
    }

    private void initSettings() {
        mActionNotificationSwitch.setChecked(mUserDto.isPromoNotification());
        mActionNotificationSwitch
                .setOnCheckedChangeListener((buttonView, isChecked) -> mPresenter.switchPromo(isChecked));

        mOrderNotificationSwitch.setChecked(mUserDto.isOrderNotification());
        mOrderNotificationSwitch
                .setOnCheckedChangeListener(((buttonView, isChecked) -> mPresenter.switchOrder(isChecked)));
    }

    private void initList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mAddressList.setLayoutManager(layoutManager);
        // TODO: 07.12.2016 create adapter
    }

    private void initProfileInfo() {
        mProfileName.setText(mUserDto.getFullname());
        mProfilePhone.setText(mUserDto.getPhone());
        /*mPicasso.load(mUserDto.getAvatar())
                .into(mUserAvatar);*/
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    //endregion

    //region ==================== IAccountView ====================

    @Override
    public void changeState() {
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            mScreen.setCustomState(EDIT_STATE);
        } else {
            mScreen.setCustomState(PREVIEW_STATE);
        }
        showViewFromState();
    }

    @Override
    public void showEditState() {
        // TODO: 07.12.2016 вот тут надо доделать "Жизнь без фрагментов" 2:46:30
//        profileNameEditText.setVisability(Visible)
        mUserAvatar.setVisibility(GONE);
        mTakePhotoButton.setVisibility(VISIBLE);
        mFab.setImageResource(R.drawable.ic_done_white_24dp);
    }

    @Override
    public void showPreviewState() {
        mUserAvatar.setVisibility(VISIBLE);
        mTakePhotoButton.setVisibility(GONE);
        mFab.setImageResource(R.drawable.ic_edit_white_24dp);
    }

    @Override
    public String getUserName() {
        return String.valueOf(mProfileName.getText());
    }

    @Override
    public String getUserPhone() {
        return String.valueOf(mProfilePhone.getText());
    }

    @Override
    public void showPhotoSourceDialog() {
        String[] source = {"Загрузить из галлереи", "Сделать фото", "Отмена"};
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Установить фото");
        alertDialog.setOnDismissListener(dialog -> mScreen.setDialogShowed(false));
        alertDialog.setItems(source, (dialog, which) -> {
            switch (which) {
                case 0:
                    mPresenter.chooseGallery();
                    break;
                case 1:
                    mPresenter.chooseGallery();
                    break;
                case 2:
                    dialog.dismiss();
                    break;
            }
            mScreen.setDialogShowed(false);
        });
        alertDialog.show();
        mScreen.setDialogShowed(true);
    }

    @Override
    public boolean viewOnBackPressed() {
        if (mScreen.getCustomState() == EDIT_STATE) {
            changeState();
            return true;
        }
        return false;
    }

    //endregion

    //region ==================== Event ====================

    @OnClick(R.id.fab)
    void clickOnEdit() {
        mPresenter.switchViewState();
    }

    @OnClick(R.id.take_photo_button)
    void clickOnPhotoEdit() {
        mPresenter.takePhoto();
    }

    // TODO: 11.12.2016 delete item address by swipe
    @OnClick(R.id.add_address_button)
    void clickOnAddAddress() {
        mPresenter.clickOnAddress();
    }
    //endregion
}
