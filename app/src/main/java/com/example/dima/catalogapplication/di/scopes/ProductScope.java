package com.example.dima.catalogapplication.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * @user dima
 * @date 15.11.2016.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ProductScope {
}
