package com.example.dima.catalogapplication.data.network;

import com.example.dima.catalogapplication.data.network.response.ProductResponse;
import com.example.dima.catalogapplication.utils.ConstantManager;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import rx.Observable;

/**
 * @user dima
 * @date 15.11.2016.
 */

public interface RestService {

//    @GET("error/400")
    @GET("products")
    Observable<Response<List<ProductResponse>>> getProducts(
            @Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastEntityUpdate);
}
