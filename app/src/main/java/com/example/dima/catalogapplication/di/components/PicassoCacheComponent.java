package com.example.dima.catalogapplication.di.components;

import com.example.dima.catalogapplication.di.modules.PicassoCacheModule;
import com.example.dima.catalogapplication.di.scopes.RootScope;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @user dima
 * @date 15.11.2016.
 */
@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoCacheComponent {
    Picasso getPicasso();
}
