package com.example.dima.catalogapplication.data.network;

import com.example.dima.catalogapplication.data.DataManager;
import com.example.dima.catalogapplication.data.network.error.ErrorUtils;
import com.example.dima.catalogapplication.data.network.error.NetworkAvailableError;
import com.example.dima.catalogapplication.utils.ConstantManager;
import com.example.dima.catalogapplication.utils.NetworkStatusChecker;
import com.fernandocejas.frodo.annotation.RxLogObservable;

import retrofit2.Response;
import rx.Observable;

/**
 * @user dima
 * @date 19.12.2016.
 */

public class RestCallTransformer<R> implements Observable.Transformer<Response<R>, R> {
    private static final int RESPONSE_CODE_OK = 200;
    private static final int RESPONSE_CODE_EMPTY_UPDATE = 304;

    @Override
    @RxLogObservable
    public Observable<R> call(Observable<Response<R>> responseObservable) {
        return NetworkStatusChecker.isInternetAvailable()
                .flatMap(aBoolean ->
                        aBoolean ? responseObservable : Observable.error(new NetworkAvailableError()))
                .flatMap(rResponse -> {
                    switch (rResponse.code()) {
                        case RESPONSE_CODE_OK:
                            /*String lastModified = rResponse.headers().get(ConstantManager.LAST_MODIFIED_HEADER);
                            if (lastModified != null) {
                                DataManager.getInstance().getPreferencesManager().saveLastProductUpdate(lastModified);
                            }*/
                            return Observable.just(rResponse.body());
                        case RESPONSE_CODE_EMPTY_UPDATE:
                            return Observable.empty();
                        default:
                            return Observable.error(ErrorUtils.parseError(rResponse));
                    }
                });
    }
}
