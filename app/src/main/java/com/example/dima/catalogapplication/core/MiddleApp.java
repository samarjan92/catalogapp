package com.example.dima.catalogapplication.core;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;

import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.components.AppComponent;
import com.example.dima.catalogapplication.di.components.DaggerAppComponent;
import com.example.dima.catalogapplication.di.modules.AppModule;
import com.example.dima.catalogapplication.di.modules.PicassoCacheModule;
import com.example.dima.catalogapplication.di.modules.RootModule;
import com.example.dima.catalogapplication.feature.root.DaggerRootActivity_RootComponent;
import com.example.dima.catalogapplication.feature.root.RootActivity;
import com.example.dima.catalogapplication.mortar.ScreenScoper;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.realm.Realm;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

/**
 * @user dima
 * @date 30.10.2016.
 */

public class MiddleApp extends Application {

    private static AppComponent sAppComponent;
    private MortarScope mRootScope;
    private MortarScope mRootActivityScope;
    private RootActivity.RootComponent mRootActivityRootComponent;
    private static Context sContext;

    public static Context getContext() {
        return sContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                    .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                    .build()
        );

        sContext = this;

        createAppComponent();
        createRootActivityComponent();

        mRootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, sAppComponent)
                .build("Root");

        mRootActivityScope = mRootScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, mRootActivityRootComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                .build(RootActivity.class.getName());

        ScreenScoper.registerScope(mRootScope);
        ScreenScoper.registerScope(mRootActivityScope);

        Fresco.initialize(this);
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    private void createAppComponent() {
        sAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext()))
                .build();
    }

    private void createRootActivityComponent() {
        mRootActivityRootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(getAppComponent())
                .rootModule(new RootModule())
                .picassoCacheModule(new PicassoCacheModule())
                .build();
    }

    @Override
    public Object getSystemService(String name) {
        return mRootScope.hasService(name) ? mRootScope.getService(name) : super.getSystemService(name);
    }
}
