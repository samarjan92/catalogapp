package com.example.dima.catalogapplication.di.modules;

import com.example.dima.catalogapplication.di.scopes.RootScope;
import com.example.dima.catalogapplication.feature.root.RootPresenter;

import dagger.Provides;

/**
 * @user dima
 * @date 03.12.2016.
 */
@dagger.Module
public class RootModule {
    @Provides
    @RootScope
    RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }
}
