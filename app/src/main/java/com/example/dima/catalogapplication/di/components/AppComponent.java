package com.example.dima.catalogapplication.di.components;

import android.content.Context;

import com.example.dima.catalogapplication.di.modules.AppModule;

import dagger.Component;

/**
 * @user dima
 * @date 15.11.2016.
 */
@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
