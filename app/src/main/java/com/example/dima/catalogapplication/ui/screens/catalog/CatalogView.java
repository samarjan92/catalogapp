package com.example.dima.catalogapplication.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.data.storage.dto.ProductDto;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.feature.catalog.CatalogContract;
import com.example.dima.catalogapplication.mvp.view.AbstractView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;
import me.relex.circleindicator.CircleIndicator;

/**
 * @user dima
 * @date 04.12.2016.
 */
public class CatalogView extends AbstractView<CatalogScreen.CatalogPresenter> implements CatalogContract.IView {

    private static final int MIN_COUNT_IN_CART_TO_SHOW = 0;
    private static final int MAX_COUNT_IN_CART_TO_SHOW = 99;

    private int mAllProductInCart = 0;

    @Inject
    CatalogScreen.CatalogPresenter mPresenter;

    CatalogScreen mScreen;

    @BindView(R.id.product_pager)
    ViewPager mProductPager;
    @BindView(R.id.indicator)
    CircleIndicator mIndicator;
    @BindView(R.id.add_to_cart_button)
    Button mAddToCartButton;

    public CatalogView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        mScreen = Flow.getKey(this);
        DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
    }


    @OnClick(R.id.add_to_cart_button)
    void clickAddToCart() {
        mPresenter.clickOnBuyButton(mProductPager.getCurrentItem());
    }

    //region ==================== ICatalogView ====================

    @Override
    public void showCatalogView(List<ProductDto> productList) {
        CatalogAdapter catalogAdapter = new CatalogAdapter();
        if (productList == null) return;
        for (ProductDto product : productList) {
            catalogAdapter.addItem(product);
        }
        mProductPager.setAdapter(catalogAdapter);
        mIndicator.setViewPager(mProductPager);
    }

    @Override
    public void updateProductCounter() {

    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion
}
