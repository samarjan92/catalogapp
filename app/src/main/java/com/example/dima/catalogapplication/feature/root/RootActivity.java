package com.example.dima.catalogapplication.feature.root;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.dima.catalogapplication.BuildConfig;
import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.base.IBaseView;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.components.AppComponent;
import com.example.dima.catalogapplication.di.modules.PicassoCacheModule;
import com.example.dima.catalogapplication.di.modules.RootModule;
import com.example.dima.catalogapplication.di.scopes.RootScope;
import com.example.dima.catalogapplication.feature.account.AccountFragment;
import com.example.dima.catalogapplication.feature.authentication.AuthActivity;
import com.example.dima.catalogapplication.flow.TreeKeyDispatcher;
import com.example.dima.catalogapplication.mvp.presenter.MenuItemHolder;
import com.example.dima.catalogapplication.mvp.view.IActionBarView;
import com.example.dima.catalogapplication.ui.screens.account.AccountScreen;
import com.example.dima.catalogapplication.ui.screens.auth.AuthScreen;
import com.example.dima.catalogapplication.ui.screens.catalog.CatalogScreen;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Direction;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

/**
 * @user dima
 * @date 29.10.2016.
 */

public class RootActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        IRootView, IActionBarView {
    private TextView mCartBager;

    @Inject
    RootPresenter mPresenter;

    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;
    @BindView(R.id.appbar_layout)
    AppBarLayout mAppBarLayout;
    private ActionBarDrawerToggle mToggler;
    private ActionBar mActionBar;
    private List<MenuItemHolder> mActionBarMenuItems;

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new CatalogScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        ButterKnife.bind(this);

        RootComponent rootComponent = DaggerService.getDaggerComponent(this);
        rootComponent.inject(this);


        initDrawer();
        intiToolbar();



        mPresenter.takeView(this);

        // TODO: 23.11.2016 initView


        setTitle("");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView(this);
        super.onDestroy();
    }

    private void initDrawer() {
        setSupportActionBar(mToolbar);
    }

    private void intiToolbar() {
        mActionBar = getSupportActionBar();
        mToggler = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(mToggler);
        mToggler.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    //region ==================== Activity Callback ====================

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        item.setChecked(true);
        Object key = null;
        switch (item.getItemId()) {
            case R.id.nav_account:
                key = new AccountScreen();
                break;
            case R.id.nav_catalog:
                key = new CatalogScreen();
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_notification:
                break;
            case R.id.nav_orders:
                break;
        }
        if (key != null) {
            Flow.get(this).replaceHistory(key, Direction.REPLACE);
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && Flow.get(this).goBack()) {
            super.onBackPressed();
        }
    }

    //endregion

    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    //region ==================== IBaseView ====================

    @Override
    public void showMessage(String message) {
        Snackbar.make(mDrawerLayout.getRootView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.auth_activity_custom_message));
            //TODO
        }
    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    @Nullable
    @Override
    public IBaseView getCurrentScreen() {
        return (IBaseView) mRootFrame.getChildAt(0);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion

    //region ==================== ActionBar setting ====================


    @Override
    public void setVisable(boolean visable) {

    }

    @Override
    public void setBackArrow(boolean enabled) {
        if (mToggler != null && mActionBar != null) {
            if (enabled) {
                mToggler.setDrawerIndicatorEnabled(false);
                mActionBar.setDisplayHomeAsUpEnabled(true);
                if (mToggler.getToolbarNavigationClickListener() == null) {
                    mToggler.setToolbarNavigationClickListener(view -> onBackPressed());
                }
            } else {
                mActionBar.setDisplayHomeAsUpEnabled(false);
                mToggler.setDrawerIndicatorEnabled(true);
                mToggler.setToolbarNavigationClickListener(null);
            }

            mDrawerLayout.setDrawerLockMode(
                    enabled ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_UNLOCKED
            );
            mToggler.syncState();
        }
    }

    @Override
    public void setMenuItems(List<MenuItemHolder> items) {
        mActionBarMenuItems = items;
        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mActionBarMenuItems != null && !mActionBarMenuItems.isEmpty()) {
            for (MenuItemHolder menuItem : mActionBarMenuItems) {
                MenuItem item = menu.add(menuItem.getItemTitle());
                item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)
                        .setIcon(menuItem.getIconResId())
                        .setOnMenuItemClickListener(menuItem.getListener());
            }
        } else {
            menu.clear();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTabLayout(ViewPager pager) {
        TabLayout tabView = new TabLayout(this);
        tabView.setupWithViewPager(pager);
        mAppBarLayout.addView(tabView);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabView));
    }

    @Override
    public void removeTabLayout() {
        View tabView = mAppBarLayout.getChildAt(1);
        if (tabView != null && tabView instanceof TabLayout) {
            mAppBarLayout.removeView(tabView);
        }
    }

    //endregion

    //region ==================== DI ====================

    @dagger.Component(dependencies = AppComponent.class, modules = { RootModule.class, PicassoCacheModule.class })
    @RootScope
    public interface RootComponent {
        void inject(AuthActivity authActivity);
        void inject(RootActivity rootActivity);

        RootPresenter getRootPresenter();
        Picasso getPicasso();
    }

    //endregion
}
