package com.example.dima.catalogapplication.data;

import android.util.Log;

import com.example.dima.catalogapplication.core.MiddleApp;
import com.example.dima.catalogapplication.data.network.RestCallTransformer;
import com.example.dima.catalogapplication.data.network.RestService;
import com.example.dima.catalogapplication.data.network.response.ProductResponse;
import com.example.dima.catalogapplication.data.storage.dto.ProductDto;
import com.example.dima.catalogapplication.data.storage.dto.ProductLocalInfo;
import com.example.dima.catalogapplication.data.storage.dto.UserAddressDto;
import com.example.dima.catalogapplication.data.storage.dto.UserDto;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.components.DaggerDataManagerComponent;
import com.example.dima.catalogapplication.di.components.DataManagerComponent;
import com.example.dima.catalogapplication.di.modules.LocalModule;
import com.example.dima.catalogapplication.di.modules.NetworkModule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by dima on 23.10.2016.
 *
 */

public class DataManager {
    private static final String TAG = DataManager.class.getName();

    private static DataManager ourInstance = new DataManager();
    @Inject
    PreferencesManager mPreferencesManager;
    @Inject
    RestService mRestService;
    @Inject
    Retrofit mRetrofit;

    private List<ProductDto> mMockProductList;
    private UserDto mMockUser;

    private boolean authUser;


    public static DataManager getInstance() {
        return ourInstance;
    }

    private DataManager() {
        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if (component == null) {
            component = DaggerDataManagerComponent.builder()
                    .appComponent(MiddleApp.getAppComponent())
                    .localModule(new LocalModule())
                    .networkModule(new NetworkModule())
                    .build();
            DaggerService.registerComponent(DataManagerComponent.class, component);
        }
        component.inject(this);
        generateMockData();
    }

    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public Observable<ProductResponse> getProductObsFromNetwork() {
        return mRestService.getProducts(mPreferencesManager.getLastProductUpdate())
                .compose(new RestCallTransformer<>()) //трансформируем response, выбрасываем APiError в случае ошибки
                .doOnNext(productResponses -> Log.e(TAG, "getProductObsFromNetwork: " + Thread.currentThread().getName()))
                .flatMap(Observable::from)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .doOnNext(productResponse -> {
                    if (!productResponse.isActive()) {
                        deleteFromDB(productResponse);
                    }
                })
                .filter(ProductResponse::isActive)
                .doOnNext(this::saveOnDisk);
//                .doOnCompleted(this::generateMockData);
    }

    private void deleteFromDB(ProductResponse productResponse) {
        Iterator<ProductDto> iterator = mMockProductList.iterator();
        while (iterator.hasNext()) {
            ProductDto productDto = iterator.next();
            if (productDto.getId() == productResponse.getRemoteId()) {
                Log.e(TAG, "deleteFromDB: " + productDto.getProductName());
                iterator.remove();
                return;
            }
        }
    }

    private void saveOnDisk(ProductResponse productResponse) {
        if (mMockProductList != null) {
            mMockProductList.add(new ProductDto(productResponse, new ProductLocalInfo()));
        }
    }

    // region ======================== Shared Preferences =====================

    public void saveToken(String token) {
        mPreferencesManager.saveToken(token);
    }

    public String getToken() {
        return mPreferencesManager.getToken();
    }

    // endregion

    public ProductDto getProductById(int productId) {
        // TODO: 29.10.2016 this temp semple mock data pls fix me (may be loaded from db)
        for (ProductDto product : mMockProductList) {
            if (productId == product.getId()) {
                return product;
            }
        }
        return null;
    }

    public void updateProduct(ProductDto product) {
        // TODO: 29.10.2016 update product count ir status (somthing in product) save in db
    }

    public void updateProductLocalInfo(ProductLocalInfo pli) {
        for (ProductDto product : mMockProductList) {
            if (pli.getRemoteId() == product.getId()) {
                product.setFavorite(pli.isFavorite());
                product.setCount(pli.getCount());
                return;
            }
        }
    }

    public List<ProductDto> getProductList() {
        // TODO: 29.10.2016 load product list from anywhere
        return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(3, "D Canon EOS 1200D Kit", "https://mdata.yandex.net/i?path=b0501093434_img_id2742888421040037634.jpeg", "любительская зеркальная фотокамера", 24990, 1, false));
        mMockProductList.add(new ProductDto(5, "D Polar M400", "https://mdata.yandex.net/i?path=b0309230140_img_id314530015396332208.jpeg", "умные часы", 11490, 1, false));
        mMockProductList.add(new ProductDto(6, "D LG Nexus 5X H791 16Gb", "https://mdata.yandex.net/i?path=b1029141711_img_id368240118241113168.jpeg", "смартфон, Android 6.0", 26690, 1, false));
        mMockProductList.add(new ProductDto(7, "D Xiaomi Mi Band 2", "https://mdata.yandex.net/i?path=b0608112127_img_id5798630194831018000.jpeg", "фитнес-браслет", 3190, 1, false));
        mMockProductList.add(new ProductDto(8, "D Samsung UE40KU6000K", "https://mdata.yandex.net/i?path=b0601192941_img_id945869098885281252.jpeg&size=9", "ЖК-телевизор, 4K UHD", 37990, 1, false));

        List<UserAddressDto> mockAdresses = new ArrayList<>();
        mockAdresses.add(new UserAddressDto(0, "Рабочий", "Мелькомбинат", "3", "1", 1, "Вход с торца жилого дома"));
        mMockUser = new UserDto("Константин", null, "89525294824", false, true, mockAdresses);
    }

    public boolean isAuthUser() {
        // TODO: 30.10.2016 check auth user in shared preference
        return true;
    }

    public void saveProfileInfo(String fullname, String phone) {
        mPreferencesManager.saveString(PreferencesManager.PROFILE_FULL_NAME_KEY, fullname);
        mPreferencesManager.saveString(PreferencesManager.PROFILE_PHONE_KEY, phone);
    }

    public void saveSetting(String settingKey, boolean isChecked) {
        mPreferencesManager.saveBoolean(settingKey, isChecked);
        if (settingKey.equals(PreferencesManager.NOTIFICATION_ACTION_KEY)) {
            mMockUser.setPromoNotification(isChecked);
        } else if (settingKey.equals(PreferencesManager.NOTIFICATION_ORDER_KEY)) {
            mMockUser.setOrderNotification(isChecked);
        }
    }

    public void addAddress(UserAddressDto userAddressDto) {
        if (mMockUser != null) {
            mMockUser.getUserAddresses().add(userAddressDto);
        }
    }

    public Map<String, String> getUserProfileInfo() {
        Map<String, String> userProfileInfo = new HashMap<>();
        String fullname = mPreferencesManager.getString(PreferencesManager.PROFILE_FULL_NAME_KEY);
        if (fullname == null) {
            fullname = "Константин Константинович";
        }
        userProfileInfo.put(PreferencesManager.PROFILE_FULL_NAME_KEY,
                fullname);
        String userPhone = mPreferencesManager.getString(PreferencesManager.PROFILE_PHONE_KEY);
        if (userPhone == null) {
            userPhone = "89525294824";
        }
        userProfileInfo.put(PreferencesManager.PROFILE_PHONE_KEY,
                userPhone);
        return userProfileInfo;
    }


    public List<UserAddressDto> getUserAddresses() {
        return mMockUser.getUserAddresses();
    }

    public Map<String,Boolean> getUserSettings() {
        Map<String, Boolean> userSettings = new HashMap<>();
        userSettings.put(PreferencesManager.NOTIFICATION_ACTION_KEY,
                mPreferencesManager.getBoolean(PreferencesManager.NOTIFICATION_ACTION_KEY));
        userSettings.put(PreferencesManager.NOTIFICATION_ORDER_KEY,
                mPreferencesManager.getBoolean(PreferencesManager.NOTIFICATION_ORDER_KEY));
        return userSettings;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public Observable<ProductLocalInfo> getProductLocalInfoObs(ProductResponse productResponse) {
        return Observable.just(getPreferencesManager().getLocalInfo(productResponse.getRemoteId()))
                .flatMap(productLocalInfo ->
                        productLocalInfo == null ?
                        Observable.just(new ProductLocalInfo()) :
                        Observable.just(productLocalInfo));
    }

    public List<ProductDto> fromDisk() {
        return mMockProductList;
    }

}
