package com.example.dima.catalogapplication.feature.catalog.product;

import com.example.dima.catalogapplication.data.storage.dto.ProductDto;

import java.util.HashMap;
import java.util.Map;

/**
 * @user dima
 * @date 30.10.2016.
 */

public class ProductPresenterFactory {
    private static final Map<String, ProductPresenter> sPresenterMap = new HashMap<>();

    private static void registerPresenter(ProductDto product, ProductPresenter presenter) {
        sPresenterMap.put(String.valueOf(product.getId()), presenter);
    }

    public static ProductPresenter getInstance(ProductDto product) {
        ProductPresenter productPresenter = sPresenterMap.get(String.valueOf(product.getId()));
        if (productPresenter == null) {
//            productPresenter = ProductPresenter.newInstance(product);
            registerPresenter(product, productPresenter);
        }
        return productPresenter;
    }
}
