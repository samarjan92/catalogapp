package com.example.dima.catalogapplication.data.storage.realm;

import com.example.dima.catalogapplication.data.network.response.ProductResponse;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @user dima
 * @date 07.01.2017.
 */

public class ProductRealm extends RealmObject {
    @PrimaryKey
    private String id;
    private String productName;
    private String imageUrl;
    private String description;
    private int price;
    private float rating;
    private int count = 1;
    private boolean favorite = false;
    private RealmList<CommentRealm> mCommentsRealm = new RealmList<>();

    public ProductRealm() {
    }

    public ProductRealm(ProductResponse productResponse) {
        this.id = productResponse.getId();
        this.productName = productResponse.getProductName();
        this.imageUrl = productResponse.getImageUrl();
        this.description = productResponse.getDescription();
        this.price = productResponse.getPrice();
        this.rating = productResponse.getRaiting();
    }

    public String getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public float getRating() {
        return rating;
    }

    public int getCount() {
        return count;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public RealmList<CommentRealm> getmCommentsRealm() {
        return mCommentsRealm;
    }
}
