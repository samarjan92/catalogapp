package com.example.dima.catalogapplication.data.storage.dto;

/**
 * @user dima
 * @date 18.12.2016.
 */

public class ProductLocalInfo {
    private int remoteId;
    private boolean isFavorite;
    private int count;

    public ProductLocalInfo() {
    }

    public ProductLocalInfo(int remoteId, boolean isFavorite, int count) {
        this.remoteId = remoteId;
        this.isFavorite = isFavorite;
        this.count = count;
    }

    public int getRemoteId() {
        return remoteId;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public int getCount() {
        return count;
    }

    public void addCount() {
        count++;
    }

    public void deleteCount() {
        count--;
    }

    public void setRemoteId(int id) {
        this.remoteId = id;
    }
}
