package com.example.dima.catalogapplication.di.modules;

import android.content.Context;

import com.example.dima.catalogapplication.di.scopes.RootScope;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @user dima
 * @date 15.11.2016.
 */
@Module
public class PicassoCacheModule {
    @Provides
    @RootScope
    Picasso providePicasso(Context context) {
        OkHttp3Downloader okHttp3Downloader = new OkHttp3Downloader(context);
        Picasso picasso = new Picasso.Builder(context)
                .downloader(okHttp3Downloader)
                .indicatorsEnabled(true)
                .loggingEnabled(true)
                .build();
        Picasso.setSingletonInstance(picasso);
        return picasso;
    }
}
