package com.example.dima.catalogapplication.mvp.model;

import android.net.Uri;

import com.example.dima.catalogapplication.base.AbstractModel;
import com.example.dima.catalogapplication.data.PreferencesManager;
import com.example.dima.catalogapplication.data.storage.dto.UserAddressDto;
import com.example.dima.catalogapplication.data.storage.dto.UserDto;

import java.util.List;
import java.util.Map;

/**
 * @user dima
 * @date 06.12.2016.
 */

public class AccountModel extends AbstractModel {

    public UserDto getUserDto() {
        return new UserDto(getUserProfileInfo(), getUserAddresses(), getUserSettings());
    }

    private Map<String, String> getUserProfileInfo() {
        return mDataManager.getUserProfileInfo();
    }

    private List<UserAddressDto> getUserAddresses() {
        return mDataManager.getUserAddresses();
    }

    private Map<String, Boolean> getUserSettings() {
        return mDataManager.getUserSettings();
    }

    public void saveProfileInfo(String fullname, String phone) {
        mDataManager.saveProfileInfo(fullname, phone);
    }

    public void saveAvatarPhoto(Uri avatarUri) {
        // TODO: 09.12.2016 implement this
    }

    public void savePromoNotification(boolean isChecked) {
        mDataManager.saveSetting(PreferencesManager.NOTIFICATION_ACTION_KEY, isChecked);
    }

    public void saveOrderNotification(boolean isChecked) {
        mDataManager.saveSetting(PreferencesManager.NOTIFICATION_ORDER_KEY, isChecked);
    }

    public void addAddress(UserAddressDto userAddressDto) {
        mDataManager.addAddress(userAddressDto);
    }

    // TODO: 09.12.2016 remove address
}
