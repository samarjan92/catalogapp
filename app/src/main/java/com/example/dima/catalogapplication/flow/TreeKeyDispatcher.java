package com.example.dima.catalogapplication.flow;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.feature.authentication.AuthActivity;
import com.example.dima.catalogapplication.mortar.ScreenScoper;

import java.util.Collections;
import java.util.Map;

import flow.Direction;
import flow.Dispatcher;
import flow.KeyChanger;
import flow.State;
import flow.Traversal;
import flow.TraversalCallback;
import flow.TreeKey;

/**
 * @user dima
 * @date 29.11.2016.
 */

public class TreeKeyDispatcher extends KeyChanger implements Dispatcher {
    private Activity mActivity;

    /**
     * ValueObjects, Keys or screens:
     * inKey - тот скрин, который должен появится на экране
     * outKey - скрин, который должен уйти с экрана
     */
    private Object inKey;
    @Nullable
    private Object outKey;
    private FrameLayout mRootFrame;

    public TreeKeyDispatcher(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void dispatch(Traversal traversal, TraversalCallback callback) {
        Map<Object, Context> contexts;
        State inState = traversal.getState(traversal.destination.top());
        inKey = inState.getKey();
        State outState = traversal.origin == null ? null : traversal.getState(traversal.origin.top());
        outKey = outState == null ? null : outState.getKey();

        mRootFrame = (FrameLayout) mActivity.findViewById(R.id.root_frame);
        if (inKey.equals(outKey)) {
            callback.onTraversalCompleted();
            return;
        }

        if (inKey instanceof TreeKey) {
            // TODO: 03.12.2016 implements treekey case (1:11:30)
        }

        // TODO: 03.12.2016 mortar context for screen

        Context flowContext = traversal.createContext(inKey, mActivity);
        Context mortartContext = ScreenScoper.getScreenScope((AbstractScreen)inKey).createContext(flowContext);
        contexts = Collections.singletonMap(inKey, mortartContext);
        changeKey(outState, inState, traversal.direction, contexts, callback);
    }

    @Override
    public void changeKey(@Nullable State outgoingState, State incomingState, Direction direction,
                          Map<Object, Context> incomingContexts, TraversalCallback callback) {

        //Context содержащий FlowService
        Context context = incomingContexts.get(inKey);

        //save prev view state
        if (outgoingState != null) {
            //Сохраняем уходящий скринн во Flow
            outgoingState.save(mRootFrame.getChildAt(0));
        }

        //create new screen
        Screen screen;
        screen = inKey.getClass().getAnnotation(Screen.class);
        if (screen == null) {
            throw new IllegalStateException("@Screen annotation is missing on screen " + ((AbstractScreen)inKey).getScopeName());
        } else {
            int layout = screen.value();
            LayoutInflater inflater = LayoutInflater.from(context);
            View newView = inflater.inflate(layout, mRootFrame, false);

            //restore state to new view
            incomingState.restore(newView);

            //delete old view
            if (outKey != null && !(inKey instanceof TreeKey)) {
                ((AbstractScreen) outKey).unregisterScope();
            }
            if (mRootFrame.getChildAt(0) != null) {
                mRootFrame.removeViewAt(0);
            }

            mRootFrame.addView(newView);
            callback.onTraversalCompleted();
        }
    }
}
