package com.example.dima.catalogapplication.feature.catalog.product;

import com.example.dima.catalogapplication.base.AbstractPresenter;
import com.example.dima.catalogapplication.data.storage.dto.ProductDto;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.scopes.ProductScope;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;

/**
 * @user dima
 * @date 30.10.2016.
 */
public class ProductPresenter /*extends AbstractPresenter<ProductContract.IView, ProductModel>*/ implements ProductContract.IPresenter {
    private static final String TAG = ProductPresenter.class.getName();

    private ProductDto mProduct;


    public ProductPresenter(ProductDto product) {
        /*Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);
        mProduct = product;*/
    }

    /*@Override
    public void initView() {
        if (getView() != null) {
            getView().showProductView(mProduct);
        }
    }*/

    @Override
    public void clickOnPlus() {
        /*mProduct.addProduct();
        mModel.updateProduct(mProduct);
        if (getView() != null) {
            getView().updateProductCountView(mProduct);
        }*/
    }

    @Override
    public void clickOnMinus() {
        /*if (mProduct.getCount() > 0) {
            mProduct.deleteProduct();
            mModel.updateProduct(mProduct);
            if (getView() != null) {
                getView().updateProductCountView(mProduct);
            }
        }*/
    }

    @Override
    public void clickOnShowMore() {

    }

    @Override
    public void clickOnLikeButton() {

    }


    //region ==================== DI ====================

    /*private Component createDaggerComponent() {
        return DaggerProductPresenter_Component.builder()
                .module(new Module())
                .build();
    }*/

    /*@Override
    protected void initActionBar() {

    }*/

    @dagger.Module
    class Module {
        @Provides
        @ProductScope
        ProductModel provideProductModel() {
            return new ProductModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @ProductScope
    interface Component {
        void inject(ProductPresenter productPresenter);
    }

    //endregion
}
