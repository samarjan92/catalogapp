package com.example.dima.catalogapplication.ui.screens.address;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.data.storage.dto.UserAddressDto;
import com.example.dima.catalogapplication.di.DaggerService;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;

/**
 * @author Dmitry Samaryan
 *         Date: 12.12.2016.
 */

public class AddressView extends RelativeLayout implements IAddressView{

    @Inject
    AddressScreen.AddressPresenter mPresenter;

    AddressScreen mScreen;

    public AddressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mScreen = Flow.getKey(context);
            DaggerService.<AddressScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region ==================== LifeCycle ====================

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    //endregion

    //region ======================= IAddressView =======================

    @Override
    public void showInputError() {
        // TODO: 12.12.2016 implement this
    }

    @Override
    public UserAddressDto getUserAddress() {
        // TODO: 12.12.2016 implement this
        return null;
    }

    @Override
    public boolean viewOnBackPressed() {
        Flow.get(this).goBack();
        return true;
    }

    //endregion

    //region ======================= Events =======================

    @OnClick(R.id.save_address_btn)
    void addAddress() {
        mPresenter.clickOnAddAddress();
    }

    //endregion
}
