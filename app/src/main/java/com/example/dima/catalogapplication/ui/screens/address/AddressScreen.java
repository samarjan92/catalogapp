package com.example.dima.catalogapplication.ui.screens.address;

import android.os.Bundle;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.scopes.AddressScope;
import com.example.dima.catalogapplication.flow.AbstractScreen;
import com.example.dima.catalogapplication.flow.Screen;
import com.example.dima.catalogapplication.mvp.model.AccountModel;
import com.example.dima.catalogapplication.ui.screens.account.AccountScreen;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import flow.TreeKey;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * @author Dmitry Samaryan
 *         Date: 12.12.2016.
 */

/**
 * Мы находимся
 * на экране добавления адресса, переходим в каталог через NavDrawer, дальше возвращаемся через NavDrawer
 * на скрин редактирования адреса, нажимаем бэкстэк и вернемся в каталог, т.к. до этого были в каталоге.
 * А должны вернуться на экран аккаунт, что бы такого не происходило у Flow есть интерфейс TreeKey,
 * который четко указывает, что данный ключ имеет родителя, и что при нажатии на бэкстэк нужно
 * вернуть родительский скрин, а не тот который находится в бэкстеке. Дополнительно обрабатывается в диспетчере
 * в методе changeKey
 */
@Screen(R.layout.screen_add_address)
public class AddressScreen extends AbstractScreen<AccountScreen.Component> implements TreeKey {
    @Override
    public Object createScreenComponent(AccountScreen.Component parentComponent) {
        return DaggerAddressScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new AccountScreen();
    }

    //region ======================= DI =======================

    @dagger.Module
    class Module {
        @Provides
        @AddressScope
        AddressPresenter provideAddressPresenter() {
            return new AddressPresenter();
        }
    }

    @dagger.Component(dependencies = AccountScreen.Component.class, modules = Module.class)
    @AddressScope
    public interface Component {
        void inject(AddressPresenter addressPresenter);

        void inject(AddressView addressView);
    }

    //endregion

    //region ======================= Presenter =======================

    public class AddressPresenter extends ViewPresenter<AddressView> implements IAddressPresenter {

        @Inject
        AccountModel mAccountModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((AddressScreen.Component)scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
        }

        @Override
        public void clickOnAddAddress() {
            if (getView() != null) {
                mAccountModel.addAddress(getView().getUserAddress());
                Flow.get(getView()).goBack();
            }
        }
    }

    //endregion
}
