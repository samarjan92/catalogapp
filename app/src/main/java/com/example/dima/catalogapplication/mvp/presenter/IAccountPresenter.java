package com.example.dima.catalogapplication.mvp.presenter;

/**
 * @user dima
 * @date 06.12.2016.
 */

public interface IAccountPresenter {

    void clickOnAddress();
    void switchViewState();
    void switchOrder(boolean isChecked);
    void switchPromo(boolean isChecked);
    void takePhoto();
    void chooseCamera();
    void chooseGallery();
}
