package com.example.dima.catalogapplication.feature.catalog;

import com.example.dima.catalogapplication.base.AbstractPresenter;
import com.example.dima.catalogapplication.data.storage.dto.ProductDto;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.scopes.CatalogScope;
import com.example.dima.catalogapplication.feature.root.IRootView;
import com.example.dima.catalogapplication.feature.root.RootActivity;
import com.example.dima.catalogapplication.feature.root.RootPresenter;

import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

/**
 * @user dima
 * @date 30.10.2016.
 */

public class CatalogPresenter /*extends AbstractPresenter<CatalogContract.IView>*/ implements CatalogContract.IPresenter {
    private List<ProductDto> mProductList;

    @Inject
    CatalogModel mCatalogModel;

    @Inject
    RootPresenter mRootPresenter;

    public CatalogPresenter() {
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            /*component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);*/
        }
        component.inject(this);
    }

    /*@Override
    public void initView() {
        if (mProductList == null) {
            mProductList = mCatalogModel.getProductList();
        }
        if (getView() != null) {
            getView().showCatalogView(mProductList);
        }
    }*/

    /*@Override
    public void clickOnBuyButton(int position) {
        if (getView() != null) {
            if (checkUserAuth()) {
                getRootView().showMessage("Товар " + mProductList.get(position).getProductName() + " успешно добвлен в корзину");
            } else {
//                getView().showAuthScreen();
            }
        }
    }*/

    /*private IRootView getRootView() {
        return mRootPresenter.getView();
    }*/

    @Override
    public void clickOnBuyButton(int position) {

    }

    @Override
    public boolean checkUserAuth() {
        return mCatalogModel.isUserAuth();
    }

    //region ==================== DI ====================

    /*private Component createDaggerComponent() {
        return DaggerCatalogPresenter_Component.builder()
                .module(new Module())
                .rootComponent(DaggerService.getComponent(RootActivity.RootComponent.class))
                .build();
    }*/

    @dagger.Module
    class Module {
        @CatalogScope
        @Provides
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @CatalogScope
    interface Component {
        void inject(CatalogPresenter catalogPresenter);
    }

    //endregion
}
