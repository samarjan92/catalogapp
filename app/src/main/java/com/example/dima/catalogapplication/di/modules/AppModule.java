package com.example.dima.catalogapplication.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * @user dima
 * @date 15.11.2016.
 */
@Module
public class AppModule {
    private Context mContext;

    public AppModule(Context mContext) {
        this.mContext = mContext;
    }

    @Provides
    Context provideContext() {
        return mContext;
    }
}
