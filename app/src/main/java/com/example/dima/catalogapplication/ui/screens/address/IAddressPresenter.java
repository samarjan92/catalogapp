package com.example.dima.catalogapplication.ui.screens.address;

/**
 * @author Dmitry Samaryan
 *         Date: 12.12.2016.
 */

public interface IAddressPresenter {
    void clickOnAddAddress();
}
