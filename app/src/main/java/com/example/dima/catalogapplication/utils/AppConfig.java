package com.example.dima.catalogapplication.utils;

/**
 * @user dima
 * @date 15.11.2016.
 */

public class AppConfig {
    public static final String BASE_URL = "https://skba1.mgbeta.ru/api/v1/";
    public static final long MAX_CONNECTION_TIMEOUT = 10_000;
    public static final long MAX_READ_TIMEOUT = 10_000;
    public static final long MAX_WRITE_TIMEOUT = 10_000;
}
