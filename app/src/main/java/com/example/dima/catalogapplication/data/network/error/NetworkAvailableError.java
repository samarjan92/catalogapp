package com.example.dima.catalogapplication.data.network.error;

/**
 * @user dima
 * @date 19.12.2016.
 */
public class NetworkAvailableError extends Throwable{
    public NetworkAvailableError() {
        super("Интернет недоступен. Попробуйте позднее");
    }
}
