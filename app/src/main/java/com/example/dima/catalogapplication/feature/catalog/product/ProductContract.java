package com.example.dima.catalogapplication.feature.catalog.product;

import com.example.dima.catalogapplication.base.IBaseView;
import com.example.dima.catalogapplication.data.storage.dto.ProductDto;
import com.example.dima.catalogapplication.data.storage.dto.ProductLocalInfo;

/**
 * @user dima
 * @date 29.10.2016.
 */

public interface ProductContract {

    public interface IPresenter {
        void clickOnPlus();
        void clickOnMinus();
        void clickOnShowMore();
        void clickOnLikeButton();
    }

    public interface IView extends IBaseView {
        void showProductView(ProductDto product);
        void updateProductCountView(ProductDto product);
        ProductLocalInfo getProductLocalInfo();
    }
}
