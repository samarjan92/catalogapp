package com.example.dima.catalogapplication.feature.authentication;

import android.support.annotation.Nullable;

/**
 * Created by dima on 22.10.2016.
 *
 */

public class AuthContract {
    public interface IView extends com.example.dima.catalogapplication.base.IBaseView {

        void showLoginButton();
        void hideLoginButton();

        void showCatalogScreen();

        boolean isIdle();

        String getUserEmail();
        String getUserPassword();

        void setCustomState(int state);
    }

    public interface IPresenter {
        void clickOnLogin();
        void clickOnFb();
        void clickOnVk();
        void clickOnTwitter();
        void clickOnShowCatalog();

        boolean checkUserAuth();
    }
}
