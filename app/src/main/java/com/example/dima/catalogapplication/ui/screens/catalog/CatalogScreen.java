package com.example.dima.catalogapplication.ui.screens.catalog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.base.AbstractPresenter;
import com.example.dima.catalogapplication.data.storage.dto.ProductDto;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.scopes.DaggerScope;
import com.example.dima.catalogapplication.feature.catalog.CatalogContract;
import com.example.dima.catalogapplication.feature.catalog.CatalogModel;
import com.example.dima.catalogapplication.feature.root.RootActivity;
import com.example.dima.catalogapplication.feature.root.RootPresenter;
import com.example.dima.catalogapplication.flow.AbstractScreen;
import com.example.dima.catalogapplication.flow.Screen;
import com.example.dima.catalogapplication.mvp.presenter.MenuItemHolder;
import com.example.dima.catalogapplication.ui.screens.auth.AuthScreen;
import com.example.dima.catalogapplication.ui.screens.product.ProductScreen;
import com.squareup.picasso.Picasso;

import java.util.List;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @user dima
 * @date 04.12.2016.
 */
@Screen(R.layout.screen_catalog)
public class CatalogScreen extends AbstractScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerCatalogScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }


    //region ======================= DI =======================

    @dagger.Module
    class Module {
        @Provides
        @DaggerScope(CatalogScreen.class)
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }

        @Provides
        @DaggerScope(CatalogScreen.class)
        CatalogPresenter provideCatalogPresenter() {
            return new CatalogPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DaggerScope(CatalogScreen.class)
    public interface Component {
        void inject(CatalogPresenter catalogPresenter);

        void inject(CatalogView catalogView);

        /**
         * Разметим зависимости дотупные для дочерних компонент, т.е. каждый ScreenProduct будет
         * иметь доступ к CatalogModel и Picasso
         */
        CatalogModel getCatalogModel();

        Picasso getPicasso();

        RootPresenter getRootPresenter();
    }

    //endregion

    //region ======================= Presenter =======================

    public class CatalogPresenter extends AbstractPresenter<CatalogView, CatalogModel> implements CatalogContract.IPresenter {
        private Subscription mProductSubs;


        @Override
        protected void initDagger(MortarScope scope) {
            ((CatalogScreen.Component)scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        /**
         * Метод, который дергается в первую очередь после того как View развернулась на экране.
         * Все методы инициализации View дергаются здесь.
         * @param savedInstanceState
         */
        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            mCompSubs.add(subscribeOnProductRealmObs());
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setBackArrow(false)
                    .setTitle("Каталог")
                    .addAction(new MenuItemHolder("В корзину", R.drawable.ic_shopping_cart_black_24dp, menuItem -> {
                        if (getRootView() != null) {
                            getRootView().showMessage("Go to cart");
                        }
                        return true;
                    }))
                    .build();
        }


        @Override
        protected void onSave(Bundle outState) {
            mProductSubs.unsubscribe();
            super.onSave(outState);
        }

        @Override
        public void clickOnBuyButton(int position) {
            /*mCatalogModel.getProductObs()
                    .doOnNext(o -> Log.e("THREAD", "call: " + Thread.currentThread().getName()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(productDto -> {
                        Log.e("PRODUCT", "from disk or network: " + productDto.getProductName());
                    });*/
            if (getView() != null) {
                if (checkUserAuth() && getRootView() != null) {
                    getRootView().showMessage("Товар " + mModel.getProductList().get(position).getProductName()
                            + " успешно добвлен в корзину");
                } else {
                    Flow.get(getView()).set(new AuthScreen());
                }
            }
        }

        private Subscription subscribeOnProductRealmObs() {
            if (getRootView() != null && getView() != null) {
                getRootView().showLoad();
                mProductSubs = mModel.getProductObs()
                        .toList()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<ProductDto>>() {
                            @Override
                            public void onCompleted() {
                                getRootView().hideLoad();
                            }

                            @Override
                            public void onError(Throwable e) {
                                getRootView().hideLoad();
                                getRootView().showError(e);
                            }

                            @Override
                            public void onNext(List<ProductDto> productDtos) {
                                getView().showCatalogView(productDtos);
                            }
                        });
            }
            // TODO: 04.01.2017 subscribe ProductObs from realm
            return mProductSubs;
        }

        @Override
        public boolean checkUserAuth() {
            return mModel.isUserAuth();
        }
    }

    //endregion

    public static class Factory {
        @SuppressLint("DefaultLocale")
        public static Context createProductContext(ProductDto product, Context parentContext) {
            MortarScope parentScope = MortarScope.getScope(parentContext);
            MortarScope childScope = null;
            ProductScreen productScreen = new ProductScreen(product);
            String scopeName = String.format("%s_%d", productScreen.getScopeName(), product.getId());

            if (parentScope.findChild(scopeName) == null) {
                childScope = parentScope.buildChild()
                        .withService(DaggerService.SERVICE_NAME,
                                productScreen.createScreenComponent(DaggerService.<CatalogScreen.Component>getDaggerComponent(parentContext)))
                        .build(scopeName);
            } else {
                childScope = parentScope.findChild(scopeName);
            }
            return childScope.createContext(parentContext);
        }
    }
}
