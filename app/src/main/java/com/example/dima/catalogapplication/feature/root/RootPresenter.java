package com.example.dima.catalogapplication.feature.root;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.example.dima.catalogapplication.base.AbstractPresenter;
import com.example.dima.catalogapplication.feature.authentication.AuthActivity;
import com.example.dima.catalogapplication.mvp.model.AccountModel;
import com.example.dima.catalogapplication.mvp.presenter.MenuItemHolder;
import com.example.dima.catalogapplication.mvp.view.IActionBarView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mortar.Presenter;
import mortar.bundler.BundleService;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * @user dima
 * @date 20.11.2016.
 */

public class RootPresenter extends Presenter<IRootView> {

    @Inject
    public AccountModel mAccountModel;
    private Subscription userInfoSub;

    private static final int DEFAULT_MODE = 0;
    private static final int TAB_MODE = 1;

    @Override
    protected BundleService extractBundleService(IRootView view) {
        return (view instanceof RootActivity) ?
                BundleService.getBundleService((RootActivity) view) : //привязываем RootPresenter к RootActivity
                BundleService.getBundleService((AuthActivity) view); //или к AuthActivity
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);

        if (getView() instanceof RootActivity) {
            userInfoSub = subscribeOnUserInfoObs();
        }
    }

    @Override
    public void dropView(IRootView view) {
        if (userInfoSub != null) {
            userInfoSub.unsubscribe();
        }
        super.dropView(view);
    }

    private Subscription subscribeOnUserInfoObs() {
        /*return mAccountModel.getUserInfoObs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new UserInfoSubscriber());*/
        return null;
    }

    public IRootView getRootView() {
        return getView();
    }

    public ActionBarBuilder newActionBarBuilder() {
        return new ActionBarBuilder();
    }

    public class ActionBarBuilder {
        private boolean isGoBack = false;
        private boolean isVisable = true;
        private CharSequence title;
        private List<MenuItemHolder> items = new ArrayList<>();
        private ViewPager pager;
        private int toolbarMode = DEFAULT_MODE;

        public ActionBarBuilder setBackArrow(boolean enable) {
            this.isGoBack = enable;
            return this;
        }

        public ActionBarBuilder setVisible(boolean enable) {
            this.isVisable = enable;
            return this;
        }

        public ActionBarBuilder setTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        public ActionBarBuilder addAction(MenuItemHolder menuItemHolder) {
            this.items.add(menuItemHolder);
            return this;
        }

        public ActionBarBuilder setTab(ViewPager viewPager) {
            this.toolbarMode = TAB_MODE;
            this.pager = viewPager;
            return this;
        }

        public void build() {
            if (getView() != null) {
                RootActivity activity = (RootActivity) getView();
                activity.setTitle(title);
                activity.setBackArrow(isGoBack);
                activity.setVisable(isVisable);
                activity.setMenuItems(items);
                if (toolbarMode == TAB_MODE) {
                    activity.setTabLayout(pager);
                } else {
                    activity.removeTabLayout();
                }
            }
        }

    }
}
