package com.example.dima.catalogapplication.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import com.example.dima.catalogapplication.core.MiddleApp;
import com.example.dima.catalogapplication.data.storage.dto.ProductLocalInfo;

/**
 * Created by dima on 23.10.2016.
 *
 */

public class PreferencesManager {

    public static final String PROFILE_FULL_NAME_KEY = "PROFILE_FULL_NAME_KEY";
    public static final String PROFILE_AVATAR_KEY = "PROFILE_AVATAR_KEY";
    public static final String PROFILE_PHONE_KEY = "PROFILE_PHONE_KEY";
    public static final String NOTIFICATION_ORDER_KEY = "NOTIFICATION_ORDER_KEY";
    public static final String NOTIFICATION_ACTION_KEY = "NOTIFICATION_ACTION_KEY";
    private static final String PRODUCT_LAST_UPDATE_KEY = "product_last_update_key";
    private final SharedPreferences mSharedPreferences;

    public PreferencesManager(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getLastProductUpdate() {
        return mSharedPreferences.getString(PRODUCT_LAST_UPDATE_KEY, "Thu, 01 Jan 1970 00:00:00 GMT");
    }

    public void saveLastProductUpdate(String lastUpdate) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PRODUCT_LAST_UPDATE_KEY, lastUpdate);
        editor.apply();
    }

    void saveToken(String token) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PreferenceConstants.USER_AUTH_TOKEN, token);
        editor.apply();
    }

    String getToken() {
        return mSharedPreferences.getString(PreferenceConstants.USER_AUTH_TOKEN, "");
    }

    void saveString(String key, String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    void saveBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    @Nullable
    public String getString(String key) {
        return mSharedPreferences.getString(key, null);
    }

    public Boolean getBoolean(String key) {
        return mSharedPreferences.getBoolean(key, false);
    }

    public ProductLocalInfo getLocalInfo(int remoteId) {
        return null;
    }
}
