package com.example.dima.catalogapplication.ui.screens.address;

import com.example.dima.catalogapplication.base.IBaseView;
import com.example.dima.catalogapplication.data.storage.dto.UserAddressDto;

/**
 * @author Dmitry Samaryan
 *         Date: 12.12.2016.
 */

public interface IAddressView extends IBaseView {

    void showInputError();
    UserAddressDto getUserAddress();
}
