package com.example.dima.catalogapplication.ui.screens.account;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.scopes.AccountScope;
import com.example.dima.catalogapplication.feature.root.IRootView;
import com.example.dima.catalogapplication.feature.root.RootActivity;
import com.example.dima.catalogapplication.feature.root.RootPresenter;
import com.example.dima.catalogapplication.flow.AbstractScreen;
import com.example.dima.catalogapplication.flow.Screen;
import com.example.dima.catalogapplication.mvp.model.AccountModel;
import com.example.dima.catalogapplication.mvp.presenter.IAccountPresenter;
import com.example.dima.catalogapplication.ui.screens.address.AddressScreen;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import mortar.ViewPresenter;

import static com.example.dima.catalogapplication.ui.screens.account.AccountView.EDIT_STATE;
import static com.example.dima.catalogapplication.ui.screens.account.AccountView.PREVIEW_STATE;

/**
 * @user dima
 * @date 06.12.2016.
 */

@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen<RootActivity.RootComponent> {
    private int mCustomState = PREVIEW_STATE;
    private boolean isDialogShowed = false;

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int mCustomState) {
        this.mCustomState = mCustomState;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAccountScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    public boolean isDialogShowed() {
        return isDialogShowed;
    }

    public void setDialogShowed(boolean mDialogShowed) {
        this.isDialogShowed = mDialogShowed;
    }

    //region ==================== DI ====================

    @dagger.Module
    public class Module {
        @Provides
        @AccountScope
        AccountPresenter provideAccountPresenter() {
            return new AccountPresenter();
        }

        @Provides
        @AccountScope
        AccountModel provideAccountModel() {
            return new AccountModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AccountScope
    public interface Component {
        void inject(AccountPresenter accountPresenter);
        void inject(AccountView accountView);

        RootPresenter getRootPresenter();
        AccountModel getAccountModel();
    }

    //endregion

    //region ==================== Presenter ====================
    public class AccountPresenter extends ViewPresenter<AccountView> implements IAccountPresenter {
        @Inject
        RootPresenter mRootPresenter;
        @Inject
        AccountModel mAccountModel;

        private Uri mAvatarUri;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((AccountScreen.Component)scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().initView(mAccountModel.getUserDto());
            }
        }

        @Override
        public void clickOnAddress() {
            Flow.get(getView()).set(new AddressScreen());
            // TODO: 09.12.2016 flow open address screen
        }

        @Override
        public void switchViewState() {
            if (getView() != null) {
                if (getCustomState() == EDIT_STATE && getView() != null) {
                    mAccountModel.saveProfileInfo(getView().getUserName(), getView().getUserPhone());
                    mAccountModel.saveAvatarPhoto(mAvatarUri);
                }
                getView().changeState();
            }
        }

        @Override
        public void switchOrder(boolean isChecked) {
            mAccountModel.saveOrderNotification(isChecked);
        }

        @Override
        public void switchPromo(boolean isChecked) {
            mAccountModel.savePromoNotification(isChecked);
        }

        @Override
        public void takePhoto() {
            if (getView() != null) {
                getView().showPhotoSourceDialog();
            }
        }

        @Override
        public void chooseCamera() {
            if (getRootView() != null) {
                getRootView().showMessage("Choose camera");
            }
            // TODO: 11.12.2016 choose from camera
        }

        @Override
        public void chooseGallery() {
            if (getRootView() != null) {
                getRootView().showMessage("Choose gallery");
            }
            // TODO: 11.12.2016 choose from gallery
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getRootView();
        }
    }
    //endregion
}
