package com.example.dima.catalogapplication.feature.catalog.product;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.core.MiddleApp;
import com.example.dima.catalogapplication.data.storage.dto.ProductDto;
import com.example.dima.catalogapplication.data.storage.dto.ProductLocalInfo;
import com.example.dima.catalogapplication.databinding.FragmentProductBinding;
import com.example.dima.catalogapplication.feature.root.RootActivity;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.components.PicassoCacheComponent;
import com.example.dima.catalogapplication.di.modules.PicassoCacheModule;
import com.example.dima.catalogapplication.di.scopes.ProductScope;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

/**
 * @user dima
 * @date 30.10.2016.
 */

public class ProductFragment extends Fragment implements ProductContract.IView, View.OnClickListener {
    private static final String BUNDLE_PRODUCT = "BUNDLE_PRODUCT";

    private FragmentProductBinding mBinding;

    @Inject
    Picasso mPicasso;

    @Inject
    ProductPresenter mPresenter;

    public ProductFragment() {}

    public static ProductFragment newInstance(ProductDto product) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_PRODUCT, product);
        ProductFragment productFragment = new ProductFragment();
        productFragment.setArguments(bundle);
        return productFragment;
    }

    private void readBundle(Bundle bundle) {
        /*if (bundle != null) {
            ProductDto product = bundle.getParcelable(BUNDLE_PRODUCT);
            Component component = createDaggerComponent(product);
            component.inject(this);
        }*/
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_product, container, false);
        View view = mBinding.getRoot();

        readBundle(getArguments());
        /*mPresenter.takeView(this);
        mPresenter.initView();*/

        mBinding.minusBtn.setOnClickListener(this);
        mBinding.plusBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        //mPresenter.dropView();
        super.onDestroyView();
    }

    //region ==================== IProductView ====================

    @Override
    public void showProductView(ProductDto product) {
        mBinding.productNameTxt.setText(product.getProductName());
        mBinding.productDescriptionTxt.setText(product.getDescription());
        mBinding.productCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mBinding.productPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice()));
        } else {
            mBinding.productPriceTxt.setText(String.valueOf(product.getPrice()));
        }
        mBinding.myImageView.setImageURI(product.getImageUrl());
        // TODO: 30.10.2016 set image to product
    }

    @Override
    public void updateProductCountView(ProductDto product) {
        mBinding.productCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mBinding.productPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice()));
        }
    }

    @Override
    public ProductLocalInfo getProductLocalInfo() {
        return null;
    }

    //endregion

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.minus_btn:
                mPresenter.clickOnMinus();
                break;
            case R.id.plus_btn:
                mPresenter.clickOnPlus();
                break;
        }
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //region ==================== DI ====================

    /*private Component createDaggerComponent(ProductDto productDto) {
        PicassoCacheComponent picassoComponent = DaggerService.getComponent(PicassoCacheComponent.class);
        if (picassoComponent == null) {
            picassoComponent = DaggerPicassoCacheComponent.builder()
                    .appComponent(MiddleApp.getAppComponent())
                    .picassoCacheModule(new PicassoCacheModule())
                    .build();
            DaggerService.registerComponent(PicassoCacheComponent.class, picassoComponent);
        }
        return DaggerProductFragment_Component.builder()
                .picassoCacheComponent(picassoComponent)
                .module(new Module(productDto))
                .build();
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @dagger.Module
    class Module {
        private ProductDto mProduct;

        public Module(ProductDto mProduct) {
            this.mProduct = mProduct;
        }

        @dagger.Provides
        @ProductScope
        ProductPresenter provideProductPresenter() {
            return new ProductPresenter(mProduct);
        }
    }

    @dagger.Component(dependencies = PicassoCacheComponent.class, modules = Module.class)
    @ProductScope
    interface Component {
        void inject(ProductFragment productFragment);
    }*/

    //endregion
}
