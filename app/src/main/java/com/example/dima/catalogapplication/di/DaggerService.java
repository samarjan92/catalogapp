package com.example.dima.catalogapplication.di;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @user dima
 * @date 14.11.2016.
 */

public class DaggerService {
    private static final String TAG = DaggerService.class.getName();
    public static final String SERVICE_NAME = "MY_DAGGER_SERVICE";

    @SuppressWarnings("uncheked")
    public static <T> T getDaggerComponent(Context context) {
        //noinspection ResourceType
        return (T) context.getSystemService(SERVICE_NAME);
    }

    private static Map<Class, Object> sComponentMap = new HashMap<>();

//    private static Map<String, Object> sComponentMap = new HashMap<>();


    // FIXME: 03.12.2016 pls
    @Nullable
    @SuppressWarnings("unchecked")
    public static <T> T getComponent(Class<T> componentClass) {
        Object component = sComponentMap.get(componentClass);
        return (T) component;
    }

    public static void registerComponent(Class componentClass, Object object) {
        sComponentMap.put(componentClass, object);
    }

    public static void unregisterComponent(Class<? extends Annotation> scopeAnnotation) {
        Iterator<Map.Entry<Class, Object>> iterator = sComponentMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Class, Object> entry = iterator.next();
            if (entry.getKey().isAnnotationPresent(scopeAnnotation)) {
                Log.d(TAG, "unregisterScope " + scopeAnnotation.getName());
                iterator.remove();
            }
        }
    }
}
