package com.example.dima.catalogapplication.feature.root;

import android.support.annotation.Nullable;

import com.example.dima.catalogapplication.base.IBaseView;

/**
 * @user dima
 * @date 20.11.2016.
 */
/*
В этом году я вел себя плохо:
Не слушался, так себе кушал
Дедушка, ты будешь в ужасе
Как в прошлом году, даже хуже
Говорил слова нехорошие,
А те что дал - все нарушил
Ноль выводов из ошибок прошлого
Только выводок новых грешков еще взял на душу
Хулиганил, дразнился, дрался, напивался, буянил
По полной жег: каким был, такоим и остался
По-другому не смог
Пожалуйста, положи для меня меня нового в свой мешок
А я залезу на тубаретку и прочту тебе свой стишок

Я все исправлю, починю и налажу
Если снег новогодний на прошлогодние грабли не ляжет
Так что я не замечу до боли знакомых зубцов под подошвой
Если следующий год снова вдруг не окажется прошлым
 */
public interface IRootView extends IBaseView{
    void showMessage(String message);
    void showError(Throwable e);

    void showLoad();
    void hideLoad();

    /**
     * Вьюха видима на экране для пользователя, с ней он взаимодействует
     * @return
     */
    @Nullable
    IBaseView getCurrentScreen();
}
