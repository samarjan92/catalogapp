package com.example.dima.catalogapplication.ui.screens.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.scopes.AuthScope;
import com.example.dima.catalogapplication.feature.authentication.AuthActivity;
import com.example.dima.catalogapplication.feature.authentication.AuthContract;
import com.example.dima.catalogapplication.feature.authentication.AuthModel;
import com.example.dima.catalogapplication.feature.root.IRootView;
import com.example.dima.catalogapplication.feature.root.RootActivity;
import com.example.dima.catalogapplication.feature.root.RootPresenter;
import com.example.dima.catalogapplication.flow.AbstractScreen;
import com.example.dima.catalogapplication.flow.Screen;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * @user dima
 * @date 29.11.2016.
 */

/**
 * Указываем какой xml-layout соответствует нашему ключу
 */
@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCustomState;

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentRootComponent) {
        return DaggerAuthScreen_Component.builder()
                .rootComponent(parentRootComponent)
                .module(new Module())
                .build();
    }

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int customState) {
        mCustomState = customState;
    }

    //region ==================== DI ====================

    @dagger.Module
    class Module {
        @Provides
        @AuthScope
        AuthPresenter providePresenter() {
            return new AuthPresenter();
        }

        @Provides
        @AuthScope
        AuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AuthScope
    interface Component {
        /**
         * В презентер впрыскиваем экземпляр модели
         * @param authPresenter
         */
        void inject(AuthPresenter authPresenter);

        /**
         * Во вью впрыскиваем экземпляр презентора
         */
        void inject(AuthView authView);
    }

    //endregion

    //region ==================== Presenter ====================

    public class AuthPresenter extends ViewPresenter<AuthView> implements AuthContract.IPresenter {

        @Inject
        AuthModel mAuthModel;
        @Inject
        RootPresenter mRootPresenter;

        /**
         * Как только наш презентер разворачивается на экране, т.е. вызывается key нашего экрана,
         * например AuthScreen.class и наш презентер попадает в видимую часть отрабатывает этот метод,
         * что означает, что мы вошли в нашу область видимости
         * @param scope из этого скоупа можем вытянуть нашу DaggerService.Component и сделать
         *              инъекции наших зависимостей
         */
        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component)scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        /**
         * Метод, который дергается в первую очередь после того как View развернулась на экране.
         * Все методы инициализации View дергаются здесь.
         * @param savedInstanceState
         */
        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null && checkUserAuth()) {
                getView().hideLoginButton();
            } else {
                getView().showLoginButton();
            }
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getRootView();
        }

        @Override
        public void clickOnLogin() {
            if (getView() != null) {
                if (getView().isIdle()) {
                    getView().setCustomState(AuthView.LOGIN_STATE);
                } else {
//                    mAuthModel.loginUser(getView().getUserEmail(), getView().getUserPassword());
                }
            }
        }

        @Override
        public void clickOnFb() {
            if (getRootView() != null) {
                getRootView().showMessage("click on fb");
            }
        }

        @Override
        public void clickOnVk() {

        }

        @Override
        public void clickOnTwitter() {

        }

        @Override
        public void clickOnShowCatalog() {
            if (getView() != null && getRootView() != null) {
                getRootView().showMessage("Show catalog");
                if (getRootView() instanceof AuthActivity) {
                    ((AuthActivity)getRootView()).startRootActivity();
                } else {
                    // TODO: 04.12.2016 show catalog screen
                }
            }
        }

        @Override
        public boolean checkUserAuth() {
            return mAuthModel.isAuthUser();
        }
    }

    //endregion
}
