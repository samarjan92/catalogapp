package com.example.dima.catalogapplication.feature.catalog.product;

import com.example.dima.catalogapplication.base.AbstractModel;
import com.example.dima.catalogapplication.data.DataManager;
import com.example.dima.catalogapplication.data.storage.dto.ProductDto;

/**
 * @user dima
 * @date 29.10.2016.
 */

public class ProductModel extends AbstractModel {

    public ProductDto getProductById(int productId) {
        // TODO: 29.10.2016 get product from from datamanager
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product) {
        mDataManager.updateProduct(product);
    }

}
