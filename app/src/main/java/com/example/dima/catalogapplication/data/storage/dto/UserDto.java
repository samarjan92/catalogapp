package com.example.dima.catalogapplication.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.dima.catalogapplication.data.PreferencesManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @user dima
 * @date 06.12.2016.
 */

public class UserDto implements Parcelable {
    private int id;
    private String fullname;
    private String avatar;
    private String phone;
    private boolean orderNotification;
    private boolean promoNotification;

    private List<UserAddressDto> userAddresses;

    public UserDto(Map<String, String> userProfileInfo, List<UserAddressDto> userAddresses, Map<String, Boolean> userSettings) {
        this.fullname = userProfileInfo.get(PreferencesManager.PROFILE_FULL_NAME_KEY);
        this.avatar = userProfileInfo.get(PreferencesManager.PROFILE_AVATAR_KEY);
        this.phone = userProfileInfo.get(PreferencesManager.PROFILE_PHONE_KEY);
        this.orderNotification = userSettings.get(PreferencesManager.NOTIFICATION_ORDER_KEY);
        this.promoNotification = userSettings.get(PreferencesManager.NOTIFICATION_ACTION_KEY);
        this.userAddresses = userAddresses;
    }

    public UserDto(String fullname, String avatar, String phone, boolean orderNotification, boolean promoNotification, List<UserAddressDto> userAddresses) {
        this.fullname = fullname;
        this.avatar = avatar;
        this.phone = phone;
        this.orderNotification = orderNotification;
        this.promoNotification = promoNotification;
        this.userAddresses = userAddresses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isOrderNotification() {
        return orderNotification;
    }

    public void setOrderNotification(boolean orderNotification) {
        this.orderNotification = orderNotification;
    }

    public boolean isPromoNotification() {
        return promoNotification;
    }

    public void setPromoNotification(boolean promoNotification) {
        this.promoNotification = promoNotification;
    }

    public List<UserAddressDto> getUserAddresses() {
        return userAddresses;
    }

    public void setUserAddresses(ArrayList<UserAddressDto> userAddresses) {
        this.userAddresses = userAddresses;
    }

    //region ==================== Parcelable ====================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(fullname);
        dest.writeString(avatar);
        dest.writeString(phone);
        dest.writeByte((byte) (orderNotification ? 1 : 0));
        dest.writeByte((byte) (promoNotification ? 1 : 0));
        dest.writeTypedList(userAddresses);
    }

    private UserDto(Parcel in) {
        id = in.readInt();
        fullname = in.readString();
        avatar = in.readString();
        phone = in.readString();
        orderNotification = in.readByte() != 0;
        promoNotification = in.readByte() != 0;
        userAddresses = in.createTypedArrayList(UserAddressDto.CREATOR);
    }

    public static final Creator<UserDto> CREATOR = new Creator<UserDto>() {
        @Override
        public UserDto createFromParcel(Parcel in) {
            return new UserDto(in);
        }

        @Override
        public UserDto[] newArray(int size) {
            return new UserDto[size];
        }
    };

    //endregion
}
