package com.example.dima.catalogapplication.feature.authentication;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.base.IBaseView;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.feature.root.IRootView;
import com.example.dima.catalogapplication.feature.root.RootActivity;
import com.example.dima.catalogapplication.feature.root.RootPresenter;
import com.example.dima.catalogapplication.flow.TreeKeyDispatcher;
import com.example.dima.catalogapplication.mortar.ScreenScoper;
import com.example.dima.catalogapplication.ui.screens.account.AccountScreen;
import com.example.dima.catalogapplication.ui.screens.auth.AuthScreen;
import com.example.dima.catalogapplication.ui.screens.catalog.CatalogScreen;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class AuthActivity extends AppCompatActivity implements IRootView {

    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;

    @Inject
    RootPresenter mRootPresenter;

    //region ==================== Life Cycle ====================

    /**
     *  В этом методе конфигурируем Flow
     * @param newBase
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
//                .defaultKey(new AuthScreen())
                .defaultKey(new CatalogScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_login);
        super.onCreate(savedInstanceState);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        ButterKnife.bind(this);

        DaggerService.<RootActivity.RootComponent>getDaggerComponent(this).inject(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        mRootPresenter.takeView(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mRootPresenter.dropView(this);
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            ScreenScoper.destroyScreenScope(RootActivity.class.getName());
        }
        super.onDestroy();
    }

    //endregion


    @Override
    public Object getSystemService(String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    public void onBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            super.onBackPressed();
        }
    }

    //region ==================== IRootView ====================

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showError(Throwable e) {
        Snackbar.make(mRootFrame, e.getMessage(), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    @Nullable
    @Override
    public IBaseView getCurrentScreen() {
        return (IBaseView) mRootFrame.getChildAt(0);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion

    public void startRootActivity() {
        Intent intent = new Intent(this, RootActivity.class);
        startActivity(intent);
        finish();
    }
}
