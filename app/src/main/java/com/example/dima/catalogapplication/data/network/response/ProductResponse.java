package com.example.dima.catalogapplication.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @user dima
 * @date 18.12.2016.
 */
public class ProductResponse {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("remoteId")
    @Expose
    private int remoteId;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private int price;
    @SerializedName("raiting")
    @Expose
    private Float raiting;
    @SerializedName("active")
    @Expose
    private Boolean active;
    @SerializedName("comments")
    @Expose
    private List<CommentResponse> comments = null;

    public String getId() {
        return id;
    }

    public int getRemoteId() {
        return remoteId;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public Float getRaiting() {
        return raiting;
    }

    public Boolean isActive() {
        return active;
    }

    public List<CommentResponse> getComments() {
        return comments;
    }
}
