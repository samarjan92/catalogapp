package com.example.dima.catalogapplication.utils;

import android.databinding.BindingAdapter;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by dima on 23.10.2016.
 *
 */

public class BindingUtils {

    @BindingAdapter({"font"})
    public static void setFont(TextView textView, String fontName){
        textView.setTypeface(Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/" + fontName));
    }
}
