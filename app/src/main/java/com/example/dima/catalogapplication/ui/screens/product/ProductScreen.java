package com.example.dima.catalogapplication.ui.screens.product;

import android.os.Bundle;
import android.util.Log;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.data.storage.dto.ProductDto;
import com.example.dima.catalogapplication.data.storage.dto.ProductLocalInfo;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.scopes.ProductScope;
import com.example.dima.catalogapplication.feature.catalog.CatalogModel;
import com.example.dima.catalogapplication.feature.catalog.product.ProductContract;
import com.example.dima.catalogapplication.feature.catalog.product.ProductModel;
import com.example.dima.catalogapplication.flow.AbstractScreen;
import com.example.dima.catalogapplication.flow.Screen;
import com.example.dima.catalogapplication.ui.screens.catalog.CatalogScreen;
import com.example.dima.catalogapplication.ui.screens.product_details.DetailScreen;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * @user dima
 * @date 05.12.2016.
 */
@Screen(R.layout.screen_product)
public class ProductScreen extends AbstractScreen<CatalogScreen.Component> {
    private ProductDto mProductDto;

    public ProductScreen(ProductDto product) {
        mProductDto = product;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ProductScreen && mProductDto.equals(((ProductScreen)o).mProductDto);
    }

    @Override
    public int hashCode() {
        return mProductDto.hashCode();
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerProductScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public String getScopeName() {
        return super.getScopeName();
    }

    //region ==================== DI ====================

    @dagger.Module
    class Module {
        @Provides
        @ProductScope
        ProductPresenter provideProductPresenter() {
            return new ProductPresenter(mProductDto);
        }
    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    @ProductScope
    interface Component {
        void inject(ProductPresenter productPresenter);

        void inject(ProductView productView);
    }

    //endregion

    //region ==================== Presenter ====================

    class ProductPresenter extends ViewPresenter<ProductView> implements ProductContract.IPresenter {

        @Inject
        CatalogModel mCatalogModel;

        private ProductDto mProduct;

        public ProductPresenter(ProductDto productDto) {
            mProduct = productDto;
            Log.e("PRODUCT CONSTRUCTOR", "ProductPresenter: " + productDto.getProductName());
        }

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((ProductScreen.Component)scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().showProductView(mCatalogModel.getProductById(mProduct.getId()));
            }
        }

        @Override
        public void clickOnPlus() {
            if (getView() != null) {
                ProductLocalInfo pli = getView().getProductLocalInfo();
                pli.setRemoteId(mProduct.getId());
                pli.addCount();
                mCatalogModel.updateProductLocalInfo(pli);
                getView().updateProductCountView(mProduct);
            }
            // FIXME: 25.12.2016 with realm db
        }

        @Override
        public void clickOnMinus() {
            if (getView() != null) {
                ProductLocalInfo pli = getView().getProductLocalInfo();
                if (pli.getCount() > 0) {
                    pli.deleteCount();
                    pli.setRemoteId(mProduct.getId());
                    mCatalogModel.updateProductLocalInfo(pli);
                    getView().updateProductCountView(mProduct);
                }
            }
        }

        @Override
        public void clickOnShowMore() {
            Flow.get(getView()).set(new DetailScreen());
        }

        @Override
        public void clickOnLikeButton() {
            if (getView() != null) {
                ProductLocalInfo pli = getView().getProductLocalInfo();
                pli.setRemoteId(mProduct.getId());
                mCatalogModel.updateProductLocalInfo(pli);
//                mCatalogModel.getProductById(mProduct.getId()).setFavorite(pli.isFavorite());
            }
        }
    }

    //endregion
}
