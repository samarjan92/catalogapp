package com.example.dima.catalogapplication.di.components;

import com.example.dima.catalogapplication.data.DataManager;
import com.example.dima.catalogapplication.di.modules.LocalModule;
import com.example.dima.catalogapplication.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @user dima
 * @date 15.11.2016.
 */
@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
