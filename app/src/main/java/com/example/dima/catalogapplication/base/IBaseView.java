package com.example.dima.catalogapplication.base;

import com.example.dima.catalogapplication.feature.authentication.AuthContract;

/**
 * @user dima
 * @date 29.10.2016.
 */

public interface IBaseView {
    boolean viewOnBackPressed();
}
