package com.example.dima.catalogapplication.feature.authentication;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

import com.example.dima.catalogapplication.BuildConfig;
import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.feature.root.RootActivity;
import com.example.dima.catalogapplication.databinding.FragmentAuthBinding;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.scopes.AuthScope;

import javax.inject.Inject;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

/**
 * @user dima
 * @date 30.10.2016.
 */

public class AuthFragment extends Fragment implements  View.OnClickListener  {
    public static final String EXTRA_IS_START_SCREEN = "EXTRA_IS_START_SCREEN";

    @Inject
    AuthPresenter mPresenter;

    FragmentAuthBinding mBinding;

    private boolean isStartScreen = true;

    private final boolean useNewInterpolators = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;

    public static AuthFragment newInstance(boolean isStartScreen) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(EXTRA_IS_START_SCREEN, isStartScreen);
        AuthFragment fragment = new AuthFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void readArguments(Bundle bundle) {
        isStartScreen = bundle.getBoolean(EXTRA_IS_START_SCREEN);
    }

    //region ==================== LifeCycle ===================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        readArguments(getArguments());

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_auth, container, false);
        View view = mBinding.getRoot();

        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);

//        mPresenter.takeView(this);
//        mPresenter.initView();

        mBinding.showCatalogBtn.setOnClickListener(this);
        mBinding.enterBtn.setOnClickListener(this);

        mBinding.loginEmailEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
//                mPresenter.typeEmail(String.valueOf(editable));
            }
        });

        mBinding.loginPasswordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
//                mPresenter.typePassword(String.valueOf(editable));
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
//        mPresenter.dropView(this);
        if (getActivity().isFinishing()) {
            DaggerService.unregisterComponent(AuthScope.class);
        }
        super.onDestroyView();
    }

    //endregion


    //region ================ IAuthView ================


    private void animateSocialIconsVisible(ViewGroup root, boolean isShown) {
        Interpolator interpolator =
                AnimationUtils.loadInterpolator(getContext(),
                        useNewInterpolators ?
                                android.R.interpolator.fast_out_slow_in : android.R.interpolator.accelerate_decelerate);
        int count = root.getChildCount();
        int delay = 100;
        for (int i = 0; i < count; i++) {
            View view = root.getChildAt(i);
            ViewPropertyAnimator vpa =view.animate();
            if (getResources().getConfiguration().orientation ==  ORIENTATION_PORTRAIT) {
                vpa.translationY(isShown ? view.getHeight() * 2 : 0f);
            } else {
                vpa.translationX(isShown ? view.getWidth() * 2 : 0f);
            }
            vpa.setInterpolator(interpolator)
                    .setDuration(600L)
                    .setStartDelay(delay * i)
                    .start();
        }
    }

    //endregion

    public boolean resolveOnBasckPressed() {
        boolean res = !mBinding.authWrapper.isIdle();
        if (res) {
            mBinding.authWrapper.setCustomState(AuthPanel.IDLE_STATE);
        }
        return res;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_catalog_btn:
//                mPresenter.clickOnShowCatalog();
                break;
            case R.id.enter_btn:
//                mPresenter.clickOnLogin();
                break;
        }
    }

    //region ==================== DI ====================

    @dagger.Module
    class Module {
        @dagger.Provides
        @AuthScope
        AuthPresenter provideAuthPresenter() {
            return new AuthPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    interface Component {
        void inject(AuthFragment authFragment);
    }

    private Component createDaggerComponent() {
        return null;
        /*return DaggerAuthFragment_Component.builder()
                .module(new Module())
                .build();*/
    }

    //endregion
}
