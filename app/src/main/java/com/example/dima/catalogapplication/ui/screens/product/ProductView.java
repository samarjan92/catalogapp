package com.example.dima.catalogapplication.ui.screens.product;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.data.storage.dto.ProductDto;
import com.example.dima.catalogapplication.data.storage.dto.ProductLocalInfo;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.feature.catalog.product.ProductContract;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @user dima
 * @date 05.12.2016.
 */

public class ProductView extends LinearLayout implements ProductContract.IView {

    @BindView(R.id.product_count_txt)
    TextView mProductCountTxt;
    @BindView(R.id.product_name_txt)
    TextView mProductNameTxt;
    @BindView(R.id.product_description_txt)
    TextView mProductDescriptionTxt;
    @BindView(R.id.product_price_txt)
    TextView mProductPriceTxt;
    @BindView(R.id.my_image_view)
    SimpleDraweeView mMyImageView;
    @BindView(R.id.favorite_btn)
    CheckBox mFavoriteBtn;
    @BindView(R.id.show_more_btn)
    Button mShowMoreBtn;

    @Inject
    ProductScreen.ProductPresenter mPresenter;

    public ProductView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<ProductScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region ==================== LifeCycle ====================

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    //endregion

    //region ==================== Events ====================

    @OnClick(R.id.minus_btn)
    void clickOnMinus() {
        mPresenter.clickOnMinus();
    }

    @OnClick(R.id.plus_btn)
    void clickOnPlus() {
        mPresenter.clickOnPlus();
    }

    @OnClick(R.id.show_more_btn)
    void clickOnShowMore() {
        mPresenter.clickOnShowMore();
    }

    @OnClick(R.id.favorite_btn)
    void clickOnFavorite() {
        mPresenter.clickOnLikeButton();
    }

    //endregion

    //region ==================== IProductView ====================

    @Override
    public void showProductView(ProductDto product) {
        mProductNameTxt.setText(product.getProductName());
        mProductDescriptionTxt.setText(product.getDescription());
        mProductCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice()));
        } else {
            mProductPriceTxt.setText(String.valueOf(product.getPrice()));
        }
        mMyImageView.setImageURI(product.getImageUrl());
        mFavoriteBtn.setChecked(product.isFavorite());
    }

    @Override
    public void updateProductCountView(ProductDto product) {
        mProductCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice()));
        }
    }

    @Override
    public ProductLocalInfo getProductLocalInfo() {
        return new ProductLocalInfo(0, mFavoriteBtn.isChecked(), Integer.parseInt(mProductCountTxt.getText().toString()));
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion

}
