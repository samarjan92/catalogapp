package com.example.dima.catalogapplication.feature.account;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.feature.root.RootActivity;

/**
 * @user dima
 * @date 30.10.2016.
 */

public class AccountFragment extends Fragment {

    public AccountFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_account, container, false);

        return rootView;
    }

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }
}
