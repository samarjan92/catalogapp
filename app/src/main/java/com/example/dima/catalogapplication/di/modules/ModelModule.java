package com.example.dima.catalogapplication.di.modules;

import com.example.dima.catalogapplication.data.DataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @user dima
 * @date 15.11.2016.
 */
@Module
public class ModelModule {
    @Provides
    @Singleton
    DataManager provideDataManager() {
        return DataManager.getInstance();
    }
}
