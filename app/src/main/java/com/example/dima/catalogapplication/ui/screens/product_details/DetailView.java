package com.example.dima.catalogapplication.ui.screens.product_details;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.mvp.view.AbstractView;

import butterknife.BindView;
import flow.Flow;

/**
 * @user dima
 * @date 04.01.2017.
 */

public class DetailView extends AbstractView<DetailScreen.DetailPresenter> {

    @BindView(R.id.detail_pager)
    ViewPager mDetailPager;

    public DetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<DetailScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    public boolean viewOnBackPressed() {
        Flow.get(this).goBack();
        return true;
    }

    public ViewPager getViewPager() {
        return mDetailPager;
    }

    public void initView() {
        DetailAdapter detailAdapter = new DetailAdapter();
        mDetailPager.setAdapter(detailAdapter);
    }
}
