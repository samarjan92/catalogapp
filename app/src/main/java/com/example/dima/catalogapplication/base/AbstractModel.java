package com.example.dima.catalogapplication.base;

import android.support.design.widget.FloatingActionButton;

import com.example.dima.catalogapplication.data.DataManager;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.components.DaggerModelComponent;
import com.example.dima.catalogapplication.di.components.ModelComponent;
import com.example.dima.catalogapplication.di.modules.ModelModule;

import javax.inject.Inject;

/**
 * @user dima
 * @date 15.11.2016.
 */

public abstract class AbstractModel {
    @Inject
    protected DataManager mDataManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(ModelComponent.class, component);
        }
        component.inject(this);
    }

    private ModelComponent createDaggerComponent() {
        return DaggerModelComponent.builder()
                .modelModule(new ModelModule())
                .build();
    }
}
