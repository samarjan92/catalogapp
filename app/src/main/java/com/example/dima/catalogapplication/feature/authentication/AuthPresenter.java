package com.example.dima.catalogapplication.feature.authentication;

import android.os.Handler;
import android.text.TextUtils;

import com.example.dima.catalogapplication.base.AbstractPresenter;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.scopes.AuthScope;

import javax.inject.Inject;

import mortar.MortarScope;
import mortar.bundler.BundleService;

/**
 * Created by dima on 22.10.2016.
 *
 */

public class AuthPresenter /*extends AbstractPresenter<AuthContract.IView, AuthModel> */{
    private static final int DEFAULT_LOGIN_TIMEOUT = 4500;
    private static final int MIN_PASSWORD_LENGTH = 4;

    @Inject
    AuthModel mAuthModel;

    public AuthPresenter() {
        /*Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);*/
    }


    /*@Override
    public void initView() {
        if (getView() != null) {
            if (checkUserAuth()) {
                getView().hideLoginButton();
            } else {
                getView().showLoginButton();
            }
        }
    }

    @Override
    protected void initActionBar() {

    }

    @Override
    protected void initDagger(MortarScope scope) {

    }

    /*@Override
    public void clickOnLogin() {
        if (getView() != null && getView().getAuthPanel() != null) {
            if (getView().getAuthPanel().isIdle()) {
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            } else {
                String email = getView().getAuthPanel().getUserEmail();
                String password = getView().getAuthPanel().getUserPassword();
                if (!isValidEmail(email)) {
                    getView().getAuthPanel().showEmailError("Введите корректный email");
                }
                if (!isValidPassword(password)) {
                    getView().getAuthPanel().showPasswordError("Введите пароль более 4 символов");
                }
                if (isValidPassword(password) && isValidEmail(email)) {
                    getView().getAuthPanel().setCustomState(AuthPanel.IDLE_STATE);
                    getView().getAuthPanel().setLoading(true);
                    getView().showLoad();
                    final Handler handler = new Handler();
                        handler.postDelayed(() -> {
                            if (getView() != null) {
                                getView().getAuthPanel().setLoading(false);
                                getView().hideLoad();
                            }
                        }, DEFAULT_LOGIN_TIMEOUT);
                }
                mAuthModel.loginUser(getView().getAuthPanel().getUserEmail(),
                        getView().getAuthPanel().getUserPassword());
//                getView().showMessage("request for user auth");
                //TODO
            }
        }
    }

    @Override
    public void clickOnFb() {
        if (getView() != null) {
            getView().showMessage("Click on FB");
        }
    }

    @Override
    public void clickOnVk() {
        if (getView() != null) {
            getView().showMessage("Click on VK");
        }
    }

    @Override
    public void clickOnTwitter() {
        if (getView() != null) {
            getView().showMessage("Click on Twitter");
        }
    }

    @Override
    public void clickOnShowCatalog() {
        if (getView() != null) {
            getView().showMessage("Показать каталог");
            // TODO: 29.10.2016 if update data comlete Start catalog screen
            getView().showCatalogScreen();
        }
    }

    @Override
    public void typeEmail(String email) {
        if (getView() != null && getView().getAuthPanel() != null) {
            if (!isValidEmail(email)) {
                getView().getAuthPanel().showEmailError("Введите корректный e-mail");
            } else {
                getView().getAuthPanel().hideEmailError();
            }
        }
    }

    @Override
    public void typePassword(String password) {
        if (getView() != null && getView().getAuthPanel() != null) {
            if (!isValidPassword(password)) {
                getView().getAuthPanel().showPasswordError("Введите пароль более 4 символов");
            } else {
                getView().getAuthPanel().hidePasswordError();
            }
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mAuthModel.isAuthUser();
    }

    private boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isValidPassword(String password) {
        return password.length() > MIN_PASSWORD_LENGTH;
    }*/

    //region ==================== DI ====================

    @dagger.Module
    class Module {
        @dagger.Provides
        @AuthScope
        AuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    interface Component {
        void inject(AuthPresenter authPresenter);
    }

    /*private Component createDaggerComponent() {
        return DaggerAuthPresenter_Component.builder()
                .module(new Module())
                .build();
    }*/

    //endregion
}
