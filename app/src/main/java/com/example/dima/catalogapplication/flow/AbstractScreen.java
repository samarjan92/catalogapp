package com.example.dima.catalogapplication.flow;

import android.util.Log;

import com.example.dima.catalogapplication.mortar.ScreenScoper;

import flow.ClassKey;

/**
 * @user dima
 * @date 29.11.2016.
 * В каждом экране нам неодходимо:
 * 1. Определить как называется область видимости
 * 2. Создавать область видимости, название которой соответствующий класс
 * 3. В каждом экране создать ScreenComponent
 *
 * Дженериком передаётся родительский компонент, т.е. компонент от которого экран будет зависеть
 */

public abstract class AbstractScreen<T> extends ClassKey {
    private static final String TAG = AbstractScreen.class.getName();

    /**
     *
     * @return название для области видимости
     */
    public String getScopeName() {
        return getClass().getName();
    }

    /**
     * 3. Создание ScreenComponent для каждого Screen c инъекцией зависимосте(компонент даггера)
     * Необходим на каждом экране для создания Dagger компоненты
     * @return
     */
    public abstract Object createScreenComponent(T parentComponent);

    // TODO: 29.11.2016 unregister scope

    /**
     * Уничтожение области видимости после ухода с экрана
     * В дальнейшем понадится в ScreenScoper
     */
    public void unregisterScope() {
        Log.d(TAG, "unregisterScopew: " + this.getScopeName());
        ScreenScoper.destroyScreenScope(getScopeName());
    }
}
