package com.example.dima.catalogapplication.data.managers;

import com.example.dima.catalogapplication.data.network.response.CommentResponse;
import com.example.dima.catalogapplication.data.network.response.ProductResponse;
import com.example.dima.catalogapplication.data.storage.realm.CommentRealm;
import com.example.dima.catalogapplication.data.storage.realm.ProductRealm;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import rx.Observable;

/**
 * @user dima
 * @date 07.01.2017.
 */

public class RealmManager {

    private Realm mRealmInstance;

    public void saveProductResponseToRealm(ProductResponse productResponse) {
        Realm realm = Realm.getDefaultInstance();

        ProductRealm productRealm = new ProductRealm(productResponse);

        if (!productResponse.getComments().isEmpty()) {
            Observable.from(productResponse.getComments())
                    .doOnNext(commentResponse -> {
                        if (!commentResponse.active) {
                            deleteFromRealm(CommentRealm.class, commentResponse.id);
                        }
                    })
                    .filter(commentResponse -> commentResponse.active)
                    .map(CommentRealm::new)
                    .subscribe(commentRealm -> {
                        productRealm.getmCommentsRealm().add(commentRealm);
                    });
        }

        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(productRealm));
        realm.close();
    }

    private void deleteFromRealm(Class<? extends RealmObject> entityRealmClass, String id) {
        Realm realm = Realm.getDefaultInstance();
        RealmObject entity = realm.where(entityRealmClass).equalTo("id", id).findFirst();

        if (entity != null) {
            realm.executeTransaction(realm1 -> entity.deleteFromRealm());
        }
        realm.close();
    }

    public Observable<ProductRealm> getAllProductFromRealm() {
        RealmResults<ProductRealm> managedProduct = queryRealmInstance().where(ProductRealm.class).findAllAsync();
        return managedProduct
                .asObservable() // получаем RealResult как Hot Observable
                .filter(RealmResults::isLoaded) //получам только загруженные результаты
                .first() // if need cold observable
                .flatMap(Observable::from); // преобразуем в Observable<ProductRealm>
    }

    private Realm queryRealmInstance() {
        if (mRealmInstance == null || mRealmInstance.isClosed()) {
            mRealmInstance = Realm.getDefaultInstance();
        }
        return mRealmInstance;
    }
}
