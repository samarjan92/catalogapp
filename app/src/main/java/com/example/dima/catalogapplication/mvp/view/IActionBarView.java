package com.example.dima.catalogapplication.mvp.view;

import android.support.v4.view.ViewPager;

import com.example.dima.catalogapplication.mvp.presenter.MenuItemHolder;

import java.util.List;

/**
 * @user dima
 * @date 04.01.2017.
 */

public interface IActionBarView {
    void setTitle(CharSequence title);
    void setVisable(boolean visable);
    void setBackArrow(boolean enabled);
    void setMenuItems(List<MenuItemHolder> items);
    void setTabLayout(ViewPager pager);
    void removeTabLayout();
}
