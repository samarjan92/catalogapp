package com.example.dima.catalogapplication.ui.screens.auth;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.feature.authentication.AuthContract;
import com.example.dima.catalogapplication.feature.authentication.AuthPanel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;

/**
 * @user dima
 * @date 30.11.2016.
 */

public class AuthView extends RelativeLayout implements AuthContract.IView {

    public static final int LOGIN_STATE = 0;
    private static final int IDLE_STATE = 1;

    @Inject
    AuthScreen.AuthPresenter mPresenter;

    @BindView(R.id.auth_card)
    CardView mAuthCard;
    @BindView(R.id.login_email_et)
    EditText mLoginEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;
    @BindView(R.id.enter_btn)
    Button mLoginBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    // TODO: 01.12.2016 дописать остальные биндинги

    private AuthScreen mScreen;

    //region ==================== LifeCycle ====================

    public AuthView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
            DaggerService.<AuthScreen.Component>getDaggerComponent(context).inject(this);
        }
        // TODO: 03.12.2016 prepare layout for album
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

        showViewFromState();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    //endregion

    private void showViewFromState() {
        if (mScreen.getCustomState() == LOGIN_STATE) {
            showLoginState();
        } else {
            showIdleState();
        }
    }

    private void showLoginState() {
        animateHideAuthCard();
        mShowCatalogBtn.setVisibility(GONE);
    }

    private void showIdleState() {
        animateShowAuthCard();
        mShowCatalogBtn.setVisibility(VISIBLE);
    }

    //region ==================== Animation ====================

    private void animateHideAuthCard() {
        boolean useNewInterpolators = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        mAuthCard.setScaleX(0f);
        mAuthCard.setScaleY(0f);
        mAuthCard.setVisibility(VISIBLE);
        Interpolator interpolator = AnimationUtils.loadInterpolator(this.getContext(),
                useNewInterpolators ?
                        android.R.interpolator.fast_out_slow_in : android.R.interpolator.accelerate_decelerate);
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(mAuthCard, View.SCALE_X, (1f));
        scaleX.setInterpolator(interpolator);
        scaleX.setDuration(200L);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(mAuthCard, View.SCALE_Y, (1f));
        scaleY.setInterpolator(interpolator);
        scaleY.setDuration(600L);
        scaleX.start();
        scaleY.start();
    }

    private void animateShowAuthCard() {
        boolean useNewInterpolators = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        Interpolator interpolator = AnimationUtils.loadInterpolator(this.getContext(),
                useNewInterpolators ?
                        android.R.interpolator.fast_out_slow_in : android.R.interpolator.accelerate_decelerate);
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(mAuthCard, View.SCALE_X, (0f));
        scaleX.setInterpolator(interpolator);
        scaleX.setDuration(200L);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(mAuthCard, View.SCALE_Y, (0f));
        scaleY.setInterpolator(interpolator);
        scaleY.setDuration(600L);
        scaleX.start();
        scaleY.start();
        //        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mAuthCard.setVisibility(GONE);
//            }
//        }, 600);
    }

    //endregion

    //region ==================== Events ====================

    @OnClick(R.id.enter_btn)
    void loginClick(View view) {
        mPresenter.clickOnLogin();
    }

    @OnClick(R.id.show_catalog_btn)
    void showCatalogClick(View view) {
        mPresenter.clickOnShowCatalog();
    }

    @OnClick(R.id.login_via_fb_btn)
    void facebookLoginClick(View view) {
        mPresenter.clickOnFb();
    }

    @OnClick(R.id.login_via_tw_btn)
    void twitterLoginClick(View view) {
        mPresenter.clickOnTwitter();
    }

    @OnClick(R.id.login_via_vk_btn)
    void vkLoginClick(View view) {
        mPresenter.clickOnVk();
    }

    //endregion

    //region ==================== IAuthView ====================

    @Override
    public void showLoginButton() {
        mLoginBtn.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoginButton() {
        mLoginBtn.setVisibility(GONE);
    }

    @Override
    public void showCatalogScreen() {

    }

    @Override
    public boolean isIdle() {
        return mScreen.getCustomState() == IDLE_STATE;
    }

    @Override
    public String getUserEmail() {
        return String.valueOf(mLoginEt.getText());
    }

    @Override
    public String getUserPassword() {
        return null;
    }

    @Override
    public void setCustomState(int state) {
        mScreen.setCustomState(state);
        showViewFromState();
    }

    @Override
    public boolean viewOnBackPressed() {
        if (!isIdle()) {
            setCustomState(IDLE_STATE);
            return true;
        }
        return false;
    }

    //endregion

}
