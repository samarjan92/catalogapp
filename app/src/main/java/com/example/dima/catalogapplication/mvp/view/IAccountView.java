package com.example.dima.catalogapplication.mvp.view;

import com.example.dima.catalogapplication.base.IBaseView;

/**
 * @user dima
 * @date 06.12.2016.
 */

public interface IAccountView extends IBaseView {

    void changeState();

    void showEditState();
    void showPreviewState();

    String getUserName();
    String getUserPhone();

    void showPhotoSourceDialog();

}
