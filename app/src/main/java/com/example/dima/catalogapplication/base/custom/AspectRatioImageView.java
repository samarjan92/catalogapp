package com.example.dima.catalogapplication.base.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.example.dima.catalogapplication.R;

/**
 * @user dima
 * @date 30.10.2016.
 */

public class AspectRatioImageView extends ImageView {

    private static final float DEFAULT_ASPECT_RATIO = 16.f / 9.f;
    private final float mAspectRatio;

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.AspectRatioImageView);
        mAspectRatio = array.getFloat(R.styleable.AspectRatioImageView_aspect_ratio,
                DEFAULT_ASPECT_RATIO);
        array.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        setMeasuredDimension(widthMeasureSpec, (int) (widthMeasureSpec / mAspectRatio));
    }
}