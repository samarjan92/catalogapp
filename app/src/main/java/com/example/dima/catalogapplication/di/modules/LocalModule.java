package com.example.dima.catalogapplication.di.modules;

import android.content.Context;

import com.example.dima.catalogapplication.data.PreferencesManager;
import com.example.dima.catalogapplication.data.managers.RealmManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @user dima
 * @date 15.11.2016.
 */
@Module
public class LocalModule {
    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context) {
        return new PreferencesManager(context);
    }

    @Provides
    @Singleton
    RealmManager provideRealmManager() {
        return new RealmManager();
    }
}
