package com.example.dima.catalogapplication.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @user dima
 * @date 18.12.2016.
 */

public class CommentResponse {
    @SerializedName("_id")
    @Expose
    public String id;
    @SerializedName("user_name")
    @Expose
    public String userName;
    @SerializedName("remoteId")
    @Expose
    public Integer remoteId;
    @SerializedName("avatar")
    @Expose
    public String avatar;
    @SerializedName("raiting")
    @Expose
    public Float raiting;
    @SerializedName("commentDate")
    @Expose
    public String commentDate;
    @SerializedName("comment")
    @Expose
    public String comment;
    @SerializedName("active")
    @Expose
    public Boolean active;
}
