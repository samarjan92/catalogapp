package com.example.dima.catalogapplication.flow;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @user dima
 * @date 29.11.2016.
 * В терминлогии ValueObject, KeyObject, Screen одни и те же вещи
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Screen {
    int value();
}
