package com.example.dima.catalogapplication.utils;

/**
 * @user dima
 * @date 18.12.2016.
 */

public class ConstantManager {
    public static final int REQUEST_PERMISSION_CAMERA = 435;
    public static final int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 631;

    public static final int REQUEST_PROFILE_PHOTO_PICKER = 726;
    public static final int REQUEST_PROFILE_PHOTO_CAMERA = 356;

    public static final String FILE_PROVIDER_AUTHORITY = "com.example.dima.catalogapplication.fileprovider";
    public static final String LAST_MODIFIED_HEADER = "Last-Modified";
    public static final String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";
}
