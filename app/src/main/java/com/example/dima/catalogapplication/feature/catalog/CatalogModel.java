package com.example.dima.catalogapplication.feature.catalog;

import com.example.dima.catalogapplication.base.AbstractModel;
import com.example.dima.catalogapplication.data.network.error.ApiError;
import com.example.dima.catalogapplication.data.network.error.NetworkAvailableError;
import com.example.dima.catalogapplication.data.network.response.ProductResponse;
import com.example.dima.catalogapplication.data.storage.dto.ProductDto;
import com.example.dima.catalogapplication.data.storage.dto.ProductLocalInfo;
import com.fernandocejas.frodo.annotation.RxLogObservable;

import java.util.List;

import rx.Observable;
import rx.functions.Func2;

/**
 * @user dima
 * @date 30.10.2016.
 */

public class CatalogModel extends AbstractModel {

    public List<ProductDto> getProductList() {
        return mDataManager.getProductList();
    }

    public boolean isUserAuth() {
        return mDataManager.isAuthUser();
    }

    public ProductDto getProductById(int productId) {
        // TODO: 29.10.2016 get product from from datamanager
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product) {
        mDataManager.updateProduct(product);
    }

    public void updateProductLocalInfo(ProductLocalInfo pli) {
        mDataManager.updateProductLocalInfo(pli);
        // TODO: 25.12.2016 realize save in DB
    }

    public Observable<ProductDto> getProductObs() {
        Observable<ProductDto> disk = getProductFromDisk();
        Observable<ProductResponse> network = getProductFromNetwork();
        Observable<ProductLocalInfo> local = network.flatMap(productResponse ->
                mDataManager.getProductLocalInfoObs(productResponse));
        Observable<ProductDto> productFromNetwork = Observable.zip(network, local, ProductDto::new);

        return Observable.merge(disk, productFromNetwork)
                .onErrorResumeNext(throwable ->
                    ((throwable instanceof NetworkAvailableError) || (throwable instanceof ApiError)) ?
                            disk :
                            Observable.error(throwable))
                .distinct(ProductDto::getId);
    }

    @RxLogObservable
    public Observable<ProductResponse> getProductFromNetwork() {
        return mDataManager.getProductObsFromNetwork();
    }

    @RxLogObservable
    public Observable<ProductDto> getProductFromDisk() {
        return Observable.defer(() -> {
            List<ProductDto> diskData = mDataManager.fromDisk();
            return diskData == null ?
                    Observable.empty() : Observable.from(diskData);
        });
    }
}
