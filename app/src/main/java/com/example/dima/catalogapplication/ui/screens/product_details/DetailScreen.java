package com.example.dima.catalogapplication.ui.screens.product_details;

import android.os.Bundle;

import com.example.dima.catalogapplication.R;
import com.example.dima.catalogapplication.base.AbstractPresenter;
import com.example.dima.catalogapplication.di.DaggerService;
import com.example.dima.catalogapplication.di.scopes.DaggerScope;
import com.example.dima.catalogapplication.flow.AbstractScreen;
import com.example.dima.catalogapplication.flow.Screen;
import com.example.dima.catalogapplication.mvp.model.DetailModel;
import com.example.dima.catalogapplication.mvp.presenter.MenuItemHolder;
import com.example.dima.catalogapplication.ui.screens.catalog.CatalogScreen;

import dagger.Provides;
import flow.TreeKey;
import mortar.MortarScope;
import mortar.bundler.BundleService;

/**
 * @user dima
 * @date 04.01.2017.
 */
@Screen(R.layout.screen_detail)
public class DetailScreen extends AbstractScreen<CatalogScreen.Component> implements TreeKey {
    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerDetailScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new CatalogScreen();
    }

    //region ==================== DI ====================

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(DetailScreen.class)
        DetailPresenter provideDetailPresenter() {
            return new DetailPresenter();
        }

        @Provides
        @DaggerScope(DetailScreen.class)
        DetailModel provideDetailModel() {
            return new DetailModel();
        }
    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = DetailScreen.Module.class)
    @DaggerScope(DetailScreen.class)
    public interface Component {
        void inject(DetailPresenter presenter);
        void inject(DetailView detailView);
    }

    //endregion

    //region ==================== Presenter ====================

    public class DetailPresenter extends AbstractPresenter<DetailView, DetailModel> {

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().initView();
            }
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                .setTitle("Product name")
                .setBackArrow(true)
                .addAction(new MenuItemHolder("В корзину", R.drawable.ic_shopping_cart_black_24dp, menuItem -> {
                    if (getRootView() != null) {
                        getRootView().showMessage("Go to backet");
                    }
                    return true;
                }))
                .setTab(getView().getViewPager())
                .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }
    }

    //endregion
}
