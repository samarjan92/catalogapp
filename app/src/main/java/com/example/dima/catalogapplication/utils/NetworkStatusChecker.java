package com.example.dima.catalogapplication.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.dima.catalogapplication.core.MiddleApp;

import rx.Observable;

/**
 * @user dima
 * @date 19.12.2016.
 */

public class NetworkStatusChecker {

    public static Boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) MiddleApp.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public static Observable<Boolean> isInternetAvailable() {
        return Observable.just(isNetworkAvailable());
    }
}
