package com.example.dima.catalogapplication.ui.screens.product_details;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dima.catalogapplication.R;

/**
 * @user dima
 * @date 04.01.2017.
 */
public class DetailAdapter extends PagerAdapter{
    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        int layout = 0;
        switch (position) {
            case 0:
                // TODO: 04.01.2017 create screen with scope
                layout = R.layout.screen_product_description;
                break;
            case 1:
                layout = R.layout.screen_comments;
                break;
        }
        View newView = LayoutInflater.from(container.getContext()).inflate(layout, container, false);
        container.addView(newView);
        return newView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(((View) object));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        switch (position) {
            case 0:
                title = "Описание";
                break;
            case 1:
                title = "Комментарии";
                break;
        }
        return title;
    }
}
