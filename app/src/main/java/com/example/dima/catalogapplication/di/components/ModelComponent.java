package com.example.dima.catalogapplication.di.components;

import com.example.dima.catalogapplication.base.AbstractModel;
import com.example.dima.catalogapplication.di.modules.ModelModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @user dima
 * @date 15.11.2016.
 */
@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
